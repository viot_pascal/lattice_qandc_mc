#ifndef _My_string_
#define _My_string_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

template <typename T> void fill_vect_from_string(std::string &str, T *&Vect) {
  if (str.empty()) {
    Vect = 0;
    std::cout << "empty str" << std::endl;
  } else {
    int found = str.find("//");
    str.erase(found);
    found = str.find("  ");
    while (found < str.length()) {
      str.erase(found, 1);
      found = str.find("  ");
    }
    if (str[str.length() - 1] == ' ')
      str.erase(str.length() - 1);
    int dim = 1;
    found = str.find(' ');
    while (found < str.length()) {
      dim++;
      found = str.find(' ', found + 1);
    }
    Vect = (T *)calloc(dim, sizeof(T));
    for (int i = 0; i < dim; i++) {
      found = str.find(' ');
      int ns = str.length();
      if (found < ns)
        ns = found;
      char temp[ns + 1];
      str.copy(temp, ns);
      temp[ns] = '\0';
      std::stringstream strValue;
      strValue << temp;
      strValue >> Vect[i];
      str.erase(0, ns + 1);
    }
  }
}

#endif