#ifndef _CONSTANT_
#define _CONSTANT_
#include <cmath>
// #include <complex>
#include <iostream>

const double pi = acos(-1.);
const double pi2 = 2 * acos(-1.);
// const std::complex<double> I (0.0,1.0);
inline double sqr(double x) { return x * x; }

#endif
