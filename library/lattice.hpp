#ifndef _LATTICE_
#define _LATTICE_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include "constant.hpp"

class Lattice {
protected:
  int Ns, Nsym; // number of sites, number of symmetry generators
  int nsitetype;
  int nn, *n_neigh, *n_cum_neigh, *neigh; // number of coupling constants,
                                          // number of neighbors for each site,
                                          // cumulated number for all sites,
                                          // table of neighbors of each site
  int *tab_L;                             // list of the link sites
  int *Lneigh; // cumulated number of links for all sites, table of links of
               // each site
  int dim;     // dimensionality of the lattice
  double *vect_basis_PFS; // double coordinates of the basis vectors in the
                          // physical (opposed to reciprocal) space
  int *coord_PFL; // coordinates in the "full" lattice (x1,x2,..,x_m_dim)
  int *site_sym;  // list of the images of the sites per symmetry
public:
  Lattice();
  ~Lattice();
  int get_dim() const;
  int get_Ns() const;
  int get_nn() const;
  int get_Nsym() const;
  int get_n_neigh(int iJ) const;
  int get_neigh(int iJ, int iS, int in) const;
  int get_nL_neigh(int iJ) const;
  bool if_cross_line(int i, int j, int d,
                     int dist) const; // says if the link ij crosses a
                                      // line/plane perpendicular to the
                                      // direction d with a length lower than
                                      // dist.
  int find_site(int *temp) const;
  void print_coord_PFL() const;
  void save_coord_PFL(char *beg_file_name, char *end_file_name);
  void print_sym(int isym) const;
  void print_neigh(int iJ) const;
  void save_Sx(char *beg_file_name, char *end_file_name, double *Sx);
  void save_Sx(char *beg_file_name, char *end_file_name, int *Sx);
  void init_links(const bool *if_filled, const int &nJ, const double *J,
                  int &nlinks, int *&n_links, int **&index_links, int *&ij_link,
                  double *&J_link, double *&scalar_link);
  int correlations_neigh_vpvm(int iJ, int *spin);
  void init_sym(std::ifstream &lattice_file);
  int image(int i, int isym);
};

struct Periodic_Lattice : public Lattice {
protected:
  int Nq, NQ; // number of wave vectors
  int m;      // number of sites in a unit cell
  int volume_cell_PBL;
  int *n; // gives the size of the unit cell in each direction
  int *coord_sites_unitcell; // integer coordinates of the m sites of the unit
                             // cell
  double *vect_basis_RBS;    // double coordinates of the basis vectors of the
                             // reciprocal lattice
  double fact_q;

public:
  int *vect_lattice; // integer coordinates of the vectors giving the extension
                     // of the lattice in the PBS basis
  int *coord_PBL;    // coordinates in the Bravais lattice (x1,x2,...,i)
  int *coord_RBL;    // coordinates of the vectors of the reciprocal space
                     // associated to the Bravais lattice
  int *coord_RFL;    // coordinates of the vectors of the reciprocal space
                     // associated to the Full lattice
  int *periodicity;  // lag the wave vectors of 2pi/periodicity[d] in the
  // direction d. periodicity=[1,1,1..] corresponds to no lag.
  Periodic_Lattice();
  ~Periodic_Lattice();
  int get_Nq() const;
  int get_NQ() const;
  void print_coord_PBL() const;
  void print_coord_RFL() const;
  void print_coord_RBL() const;
  void print_properties() const;
  void init_periodic_unit_cell(std::ifstream &lattice_file);
  void init_periodic_latt(std::ifstream &lattice_file);
  void init_sym(std::ifstream &lattice_file);
  void fill_coord_RFL(double *QQ) const;
  void fill_coord_RBL(double *Q) const;
  int find_neigh_site(int i, int *temp) const;
  int find_site(int *coord) const;
  int find_site_q(double *coord) const;
  double verif_sumrule(double *Sq) const;
  double compute_Sq(double *fx, int iq) const;
  void compute_Sq(double *fx, double *&fq);
  void save_coord_RFL(char *beg_file_name, char *end_file_name);
  void save_Sq(char *beg_file_name, char *end_file_name, double *Sq);
  void compute_Sq_powder(double *&Qp, double *&Sqp, double *Sq, int nthetap,
                         int nqp);
  void save_Sq_powder(char *beg_file_name, char *end_file_name, int nqp,
                      double *Qp, double *Sqp);
};

#endif
