#include "Wang_Landau.hpp"
#include <fstream>
#include <cstring>

Wang_Landau_histograms::Wang_Landau_histograms()
    : histo_count(0), ln_density(0) {}

Wang_Landau_histograms::Wang_Landau_histograms(double dE0, double Emin0,
                                               double Emax0, double f0)
    : dE(dE0), Emin(Emin0), Emax(Emax0), tolerance(0.9), ndone(0), f(f0) {
  Emin -= dE / 2;
  Emax += dE / 2;
  nE = int((Emax - Emin) / (dE));
  histo_count = (int *)calloc(nE, sizeof(int));
  ln_density = (double *)calloc(nE, sizeof(double));
}

Wang_Landau_histograms::~Wang_Landau_histograms() {
  free(histo_count);
  free(ln_density);
}

void Wang_Landau_histograms::print_histo() {
  for (int i = 0; i < nE; i++)
    std::cout << " histo[" << i << "]=" << histo_count[i];
  std::cout << std::endl;
}

void Wang_Landau_histograms::renormalize_corr(double *Sx, const int Ns) {
  for (int ie = 0; ie < nE; ie++) {
    double fact = 1. / histo_count[ie];
    for (int i = 0; i < Ns; i++)
      Sx[ie * Ns + i] *= fact;
  }
}

void Wang_Landau_histograms::print_all_E() {
  std::cout << "Emax=" << Emax << ", Emin=" << Emin << ", dE=" << dE
            << std::endl;
  for (int i = 0; i < nE; i++)
    std::cout << "E[" << i << "]=" << Emin + dE *i << std::endl;
}

void Wang_Landau_histograms::save_Sx(double *Sx, int const Ns,
                                     char *beg_file_name, char *end_file_name) {
  std::ofstream file;
  char file_name[100];
  sprintf(file_name, beg_file_name);
  strcat(file_name, end_file_name);
  file.open(file_name);
  if (!file) {
    std::cerr << "Unable to open the save_WLlngE file " << file_name
              << std::endl;
    exit(1);
  }
  file << "#f=" << f << std::endl;
  file << "#E Sx[] " << std::endl;
  for (int ie = 0; ie < nE; ie++) {
    file << Emin + (ie + 0.5) * dE;
    for (int i = 0; i < Ns; i++)
      file << " " << Sx[ie * Ns + i];
    file << "\n";
  }
  file.close();
}

void Wang_Landau_histograms::save_obs(double *m, char *beg_file_name,
                                      char *end_file_name) {
  std::ofstream file;
  char file_name[100];
  sprintf(file_name, beg_file_name);
  strcat(file_name, end_file_name);
  file.open(file_name);
  if (!file) {
    std::cerr << "Unable to open the save_WLm file " << file_name << std::endl;
    exit(1);
  }
  file << "#f=" << f << std::endl;
  file << "#m  " << std::endl;
  for (int i = 0; i < nE; i++) {
    file << m[i] << "\n";
  }
  file.close();
}

void Wang_Landau_histograms::save_lngE(char *beg_file_name,
                                       char *end_file_name) {

  double min = ln_density[0];
  for (int i = 1; i < nE; i++)
    if (ln_density[i] < min)
      min = ln_density[i];
  for (int i = 0; i < nE; i++)
    ln_density[i] -= min;

  std::ofstream file;
  char file_name[100];
  sprintf(file_name, beg_file_name);
  strcat(file_name, end_file_name);
  file.open(file_name);
  if (!file) {
    std::cerr << "Unable to open the save_WLlngE file " << file_name
              << std::endl;
    exit(1);
  }

  file << "#f=" << f << std::endl;
  file << "#E lng(E) histo(E)  " << std::endl;
  for (int i = 0; i < nE; i++) {
    file << Emin + (i + 0.5) * dE << " " << ln_density[i] << " "
         << histo_count[i] << "\n";
  }
  file.close();
}
