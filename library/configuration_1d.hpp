#ifndef _CONFIGURATION_
#define _CONFIGURATION_

#include <cstring>
#include "Wang_Landau.hpp"
#include "constant.hpp"
#include "lattice.hpp"
#include "random.hpp"

// structure Spin_configuration
struct Spin_configuration {
protected:
  int Ns, nspins, nlinks; // number of sites and of spins and of links
  int *spins;             // orientation of the spins
  bool *if_filled;        // 0 if the site is empty
  double *J_link;         // gives the value of the coupling for each link
  double *scalar_link;    // scalar product of two spins of a link
  int *n_links;           // number of links of a site
  int **index_links; // index_links[i][2*j] : number of the jth link associated
                     // to site i
  // index_links[i][2*j+1] : number of the site of the jth link associated to
  // site i
  int *ij_link; // ij_link[2*ilink]=i, ij_link[2*ilink+1]=j
  int *i_spins; // number of the occupied sites
  int n_potts;
  double beta;
  double T;
  double E;
  void init_energie();

public:
  Spin_configuration();
  Spin_configuration(int Ns0);
  ~Spin_configuration();
  void init(int &nJ, double *J, double &T, double filling, int n_potts,
            std::string initial_conditions, Lattice *lattice);
  void clean();
  double get_E() const;
  void print(bool b);
  void init_WL(Wang_Landau_histograms *wl);
  void WL_step(Wang_Landau_histograms *wl);
  void MC_step();
  void update_corr(double *Sx, int i0 = 0);
  void save(char *begin_file_name, char *end_file_name);
};

// scalar product of two spins
double scalar_spin(int S1, int S2) { return (S1 == S2) ? 1 : 0; }

// CONSTRUCTORS of Spin_configuration
Spin_configuration::Spin_configuration(int Ns0)
    : Ns(Ns0), nspins(0), E(0), T(0), beta(0), J_link(0), scalar_link(0),
      ij_link(0), i_spins(0) {
  spins = new int[Ns];      // orientation of the spin
  if_filled = new bool[Ns]; // 0 if the site is empty
  index_links = new int *[Ns];
  n_links = new int[Ns];
}

Spin_configuration::Spin_configuration()
    : Ns(0), n_potts(0), spins(0), nspins(0), E(0), T(0), beta(0), if_filled(0),
      J_link(0), scalar_link(0), n_links(0), index_links(0), nlinks(0),
      ij_link(0), i_spins(0) {}

// template<int d_spin> void Spin_configuration<d_spin>::init(int &nJ,double
// *J,double &T0,Lattice* latt,double filling,std::string initial_conditions){
//       config.init(params.nJ,params.J,params.T,&gLattice,params.filling,params.initial_conditions);
void Spin_configuration::init(int &nJ, double *J, double &T0, double filling,
                              int n_potts0, std::string initial_conditions,
                              Lattice *latt) {
  T = T0;
  n_potts = n_potts0;
  if (T > 0)
    beta = 1 / T;
  else {
    std::cerr << "Spin_configuration::init, T=0... not yet implemented"
              << std::endl;
    exit(1);
  }
  nspins = 0;
  nlinks = 0;
  if_filled[0] = true;
  nspins++;
  double f = (Ns * filling - 1) / (Ns - 1);
  if (initial_conditions == "random") {
    spins[0] = generate_int(n_potts);
    for (int i = 1; i < Ns; i++) {
      if_filled[i] = (generate_double() < f) ? true : false;
      if (if_filled[i]) {
        nspins++;
        spins[i] = generate_int(n_potts);
      }
    }
  } else if (initial_conditions == "uniform") {
    spins[0] = 0;
    for (int i = 1; i < Ns; i++) {
      if_filled[i] = (generate_double() < f) ? true : false;
      if (if_filled[i]) {
        nspins++;
        spins[i] = 0;
      }
    }
  } else if (initial_conditions == "bidirectionnal") {
    spins[0] = 0;
    for (int i = 2; i < Ns; i += 2) {
      if_filled[i] = (generate_double() < f) ? true : false;
      if (if_filled[i]) {
        nspins++;
        spins[i] = 1;
      }
    }
    for (int i = 1; i < Ns; i += 2) {
      if_filled[i] = (generate_double() < f) ? true : false;
      if (if_filled[i]) {
        nspins++;
        spins[i] = 1;
      }
    }
  } else if (initial_conditions == "tridirectionnal") {
    if (n_potts < 3) {
      std::cerr << "initial condition (tridirectionnal) incompatible with the "
                   "spin dimension : n_potts=" << n_potts << std::endl;
      exit(1);
    }
    spins[0] = 0;
    for (int i = 3; i < Ns; i += 3) {
      if_filled[i] = (generate_double() < f) ? true : false;
      if (if_filled[i]) {
        nspins++;
        spins[i] = 0;
      }
    }
    for (int i = 1; i < Ns; i += 3) {
      if_filled[i] = (generate_double() < f) ? true : false;
      if (if_filled[i]) {
        nspins++;
        spins[i] = 1;
      }
    }
    for (int i = 2; i < Ns; i += 3) {
      if_filled[i] = (generate_double() < f) ? true : false;
      if (if_filled[i]) {
        nspins++;
        spins[i] = 2;
      }
    }
  } else {
    std::cout << "initial conditions not defined" << std::endl;
    exit(1);
  }
  i_spins = new int[nspins];
  nspins = 0;
  for (int i = 0; i < Ns; i++) {
    if (if_filled[i]) {
      i_spins[nspins] = i;
      nspins++;
    }
  }
  latt->init_links(if_filled, nJ, J, nlinks, n_links, index_links, ij_link,
                   J_link, scalar_link);
  init_energie();
}

void Spin_configuration::clean() {
  if (i_spins) {
    delete[] i_spins;
    i_spins = 0;
  }
  if (ij_link) {
    delete[] ij_link;
    ij_link = 0;
  }
  if (scalar_link) {
    delete[] scalar_link;
    scalar_link = 0;
  }
  if (J_link) {
    delete[] J_link;
    J_link = 0;
  }
}

// DESTRUCTOR of Spin_configuration
Spin_configuration::~Spin_configuration() {
  //   if(i_spins)delete[] i_spins;
  //   if(ij_link)delete[] ij_link;
  //   if(scalar_link)delete[] scalar_link;
  //   if(J_link)delete[] J_link;
}

double Spin_configuration::get_E() const { return E; }

// print all informations on a configuration
void Spin_configuration::print(bool b = true) {
  std::cout << "number of sites Ns=" << Ns
            << ", number of spins nspins=" << nspins
            << ", number of links nlinks=" << nlinks << std::endl;
  if (b) {
    for (int i = 0; i < Ns; i++) {
      std::cout << " spin[" << i << "]";
      if (if_filled[i]) {
        std::cout << ": " << spins[i];
        std::cout << std::endl;
      } else
        std::cout << std::endl;
    }
    std::cout << std::endl;
  }
}

void Spin_configuration::init_energie() {
  E = 0;
  for (int i = 0; i < nlinks; i++) {
    scalar_link[i] =
        scalar_spin(spins[ij_link[2 * i]], spins[ij_link[2 * i + 1]]);
    E += J_link[i] * scalar_link[i];
  }
}

void Spin_configuration::MC_step() {
  for (int n = 0; n < nspins; n++) {
    int i = i_spins[gsl_rng_uniform_int(rng, nspins)];
    int S_new = generate_int(n_potts);
    //     std::cout<<S_new<<" npotts="<<n_potts<<std::endl;
    double deltaE = 0;
    double temp[n_links[i]];
    for (int j = 0; j < n_links[i]; j++) {
      temp[j] = scalar_spin(S_new, spins[index_links[i][2 * j + 1]]);
      deltaE += J_link[index_links[i][2 * j]] *
                (temp[j] - scalar_link[index_links[i][2 * j]]);
    }
    if (generate_double() < exp(-deltaE * beta)) {
      E += deltaE;
      spins[i] = S_new;
      for (int j = 0; j < n_links[i]; j++) {
        scalar_link[index_links[i][2 * j]] = temp[j];
      }
    }
  }
}

void Spin_configuration::init_WL(Wang_Landau_histograms *wl) {
  int count = 0;
  while (wl->E_not_in_range(E) and count < 10000) {
    count++;
    int i = i_spins[gsl_rng_uniform_int(rng, nspins)];
    int S_new = generate_int(n_potts);
    double deltaE = 0;
    double temp[n_links[i]];
    for (int j = 0; j < n_links[i]; j++) {
      temp[j] = scalar_spin(S_new, spins[index_links[i][2 * j + 1]]);
      deltaE += J_link[index_links[i][2 * j]] *
                (temp[j] - scalar_link[index_links[i][2 * j]]);
    }
    if (wl->is_better(E, deltaE)) {
      E += deltaE;
      spins[i] = S_new;
      for (int j = 0; j < n_links[i]; j++) {
        scalar_link[index_links[i][2 * j]] = temp[j];
      }
    }
  }
  if (count == 10000) {
    std::cout << "Maximal number of initiation iteration attained. Are Emin "
                 "and Emax ok ???" << std::endl;
    exit(1);
  }
  wl->update_iE(E);
}

void Spin_configuration::WL_step(Wang_Landau_histograms *wl) {
  int i = i_spins[gsl_rng_uniform_int(rng, nspins)];
  int S_new = generate_int(n_potts);
  double deltaE = 0;
  double temp[n_links[i]];
  for (int j = 0; j < n_links[i]; j++) {
    temp[j] = scalar_spin(S_new, spins[index_links[i][2 * j + 1]]);
    deltaE += J_link[index_links[i][2 * j]] *
              (temp[j] - scalar_link[index_links[i][2 * j]]);
  }
  if (wl->if_accept(E + deltaE)) {
    E += deltaE;
    spins[i] = S_new;
    for (int j = 0; j < n_links[i]; j++) {
      scalar_link[index_links[i][2 * j]] = temp[j];
    }
  }
}

void Spin_configuration::update_corr(double *Sx, int i0) {
  Sx[i0]++;
  //   std::cout<<"avt i0="<<i0<<" ";
  for (int i = 1; i < Ns; i++) {
    if (if_filled[i]) {
      Sx[i0 + i] += scalar_spin(spins[0], spins[i]);
    }
    //     std::cout<<" "<<i0+i<<"-"<<Sx[i0+i];
  }
  //   std::cout<<"\n";
  //   std::cout<<"ap";
}

void Spin_configuration::save(char *begin_file_name, char *end_file_name) {
  std::ofstream config_file;
  char config_file_name[100];
  sprintf(config_file_name, begin_file_name);
  strcat(config_file_name, end_file_name);
  config_file.open(config_file_name);
  if (!config_file) {
    std::cerr << "Unable to open the config file: "
              << "config_register.res" << std::endl;
    std::exit(1);
  }
  config_file << "#i S(i)" << std::endl;
  for (int i = 0; i < nspins; i++) {
    char *c;
    config_file << i_spins[i] << " " << spins[i_spins[i]] << std::endl;
  }
  config_file.close();
}

// void spin::charac(char*& c){
//   c=new char[100];
//   char ctemp[10];
//   sprintf(c," ");
//   sprintf(ctemp,"%4.3f ",s);
//   strcat(c,ctemp);
// }

#endif
