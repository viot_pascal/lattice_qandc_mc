#include "Wang_Landau_discrete_E.hpp"
#include <fstream>
#include <cstring>

Wang_Landau_histograms::Wang_Landau_histograms()
    : histo_count(0), ln_density(0) {}

Wang_Landau_histograms::Wang_Landau_histograms(int dE0, int Emin0, int Emax0,
                                               double f0)
    : dE(dE0), Emin(Emin0), Emax(Emax0), tolerance(0.9), ndone(0), f(f0) {
  nE = int((Emax - Emin) / dE);
  histo_count = (int *)calloc(nE, sizeof(int));
  ln_density = (double *)calloc(nE, sizeof(double));
}

Wang_Landau_histograms::~Wang_Landau_histograms() {
  free(histo_count);
  free(ln_density);
}

void Wang_Landau_histograms::print_histo() {
  for (int i = 0; i < nE; i++)
    std::cout << " histo[" << i << "]=" << histo_count[i];
  std::cout << std::endl;
}

void Wang_Landau_histograms::renormalize_corr(double *Sx, const int Ns) {
  for (int ie = 0; ie < nE; ie++) {
    double fact = 1. / histo_count[ie];
    for (int i = 0; i < Ns; i++)
      Sx[ie * Ns + i] *= fact;
  }
}

void Wang_Landau_histograms::print_all_E() {
  std::cout << "Emax=" << Emax << ", Emin=" << Emin << ", dE=" << dE
            << std::endl;
  for (int i = 0; i < nE; i++)
    std::cout << "E[" << i << "]=" << Emin + dE *i << std::endl;
}

void Wang_Landau_histograms::verify_dE(int deltaE) {
  if ((deltaE % dE) != 0) {
    std::cout << "dE not appropriate." << std::endl;
    exit(1);
  }
}

void Wang_Landau_histograms::adapt_E_range(int E) {
  int reste = (E - Emin) % dE;
  if (reste != 0)
    Emin -= reste;
  reste = (E - Emax) % dE;
  if (reste != 0)
    Emax -= reste;
}

void Wang_Landau_histograms::save_Sx(double *Sx, int const Ns,
                                     char *beg_file_name, char *end_file_name) {
  std::ofstream file;
  char file_name[100];
  sprintf(file_name, beg_file_name);
  strcat(file_name, end_file_name);
  file.open(file_name);
  if (!file) {
    std::cerr << "Unable to open the save_WLlngE file " << file_name
              << std::endl;
    exit(1);
  }
  file << "#f=" << f << std::endl;
  file << "#E Sx[] " << std::endl;
  for (int ie = 0; ie < nE; ie++) {
    file << Emin + (ie + 0.5) * dE;
    for (int i = 0; i < Ns; i++)
      file << " " << Sx[ie * Ns + i];
    file << "\n";
  }
  file.close();
}

void Wang_Landau_histograms::save_obs(double *m, char *beg_file_name,
                                      char *end_file_name) {
  std::ofstream file;
  char file_name[100];
  sprintf(file_name, beg_file_name);
  strcat(file_name, end_file_name);
  file.open(file_name);
  if (!file) {
    std::cerr << "Unable to open the save_WLm file " << file_name << std::endl;
    exit(1);
  }
  file << "#f=" << f << std::endl;
  file << "#m histo" << std::endl;
  for (int i = 0; i < nE; i++) {
    file << m[i] / histo_count[i];
    file << histo_count[i] << "\n";
  }
  file.close();
}

void Wang_Landau_histograms::save_obs(double *m1, double *m2, double *m3, char *beg_file_name,
                                      char *end_file_name) {
  char file_name[100];
  sprintf(file_name, beg_file_name);
  strcat(file_name, end_file_name);
  std::ofstream file(file_name, std::ios::out);
  if (!file) {
    std::cerr << "Unable to open the save_WLm file " << file_name << std::endl;
    exit(1);
  }
  file << "#f=" << f << std::endl;
  file << "#m1 m2 m3 histo" << std::endl;
  for (int i = 0; i < nE; i++) {
    file << m1[i] / histo_count[i] << " ";
    file << m2[i] / histo_count[i] << " ";
    file << m3[i] / histo_count[i] << " ";
    file << histo_count[i] << "\n";
  }
  file.close();
}

void Wang_Landau_histograms::save_lngE(char *beg_file_name,
                                       char *end_file_name) {

  double min = ln_density[0];
  for (int i = 1; i < nE; i++)
    if (ln_density[i] < min)
      min = ln_density[i];
  for (int i = 0; i < nE; i++)
    ln_density[i] -= min;

  char file_name[100];
  sprintf(file_name, beg_file_name);
  strcat(file_name, end_file_name);
  std::ofstream file(file_name, std::ios::out);
  if (!file) {
    std::cerr << "Unable to open the save_WLlngE file " << file_name
              << std::endl;
    exit(1);
  }
  file << "#f=" << f << std::endl;
  file << "#E lng(E) histo(E)  " << std::endl;
  for (int i = 0; i < nE; i++) {
    file << Emin + i *dE << " " << ln_density[i] << " " << histo_count[i]
         << "\n";
  }
  file.close();
}

void Wang_Landau_histograms::read_lngE(char *lngE_file_name){
  std::ifstream lngE_file(lngE_file_name, std::ios::in);
  if (!lngE_file) {
    std::cerr << "Unable to open the lngE file: " << lngE_file_name << std::endl;
    exit(1);
  }
  std::string str_temp;
  double E;
  for(int i=0;i<nE;i++)
    lngE_file >> E >> ln_density[i];
  lngE_file.close();
}
