#include "lattice_dev.hpp"

Lattice::Lattice()
    : Ns(0), dim(0), nn(0), Nsym(0), n_neigh(0), n_cum_neigh(0), tab_L(0),
      neigh(0), Lneigh(0), vect_basis_PFS(0), coord_PFL(0), site_sym(0) {}

Lattice::~Lattice() {
  delete vect_basis_PFS; // coordinates of the basis vectors in the physical
                         // (opposed to reciprocal) space
  //   delete site_sym;      //coordinates of the basis vectors in the physical
  //   (opposed to reciprocal) space
  delete neigh; // coordinates of the basis vectors in the physical (opposed to
                // reciprocal) space
}

Periodic_Lattice::Periodic_Lattice()
    : Lattice(), fact_q(0), Nq(0), NQ(0), n(0), m(0), volume_cell_PBL(0),
      vect_lattice(0), vect_basis_RBS(0), coord_sites_unitcell(0), coord_PBL(0),
      coord_RBL(0), coord_RFL(0) {}

Periodic_Lattice::~Periodic_Lattice() {
  delete n;         // number on sublattices in all directions
  delete coord_RBL; // coordinates of the vectors of the reciprocal space
                    // associated to the Bravais lattice
}

int Lattice::get_Ns() const { return Ns; }

int Lattice::get_nn() const { return nn; }

int Lattice::get_n_neigh(int iJ) const { return n_neigh[iJ]; }

int Lattice::get_neigh(int iJ, int iS, int in) const {
  return neigh[n_cum_neigh[iJ] + iS * n_neigh[iJ] + in];
}

int Lattice::get_nL_neigh(int iJ) const {
  return n_cum_neigh[iJ + 1] - n_cum_neigh[iJ];
}

int Lattice::get_dim() const { return dim; }

int Lattice::get_Nsym() const { return Nsym; }

bool Lattice::if_cross_line(int i, int j, int d, int dist) const {
  if (coord_PFL[j * dim + d] - coord_PFL[i * dim + d] <= dist and
      coord_PFL[i * dim + d] <= dist and coord_PFL[j * dim + d] > dist) {
    //     std::cout<<"crossline "<<i<<" "<<j<<std::endl;
    return true;
  } else
    return false;
}

int Periodic_Lattice::get_Nq() const { return Nq; }

int Periodic_Lattice::get_NQ() const { return NQ; }

void Lattice::print_coord_PFL() const {
  std::cout
      << "Display of the coordinates of the sites of the full physical lattice"
      << std::endl;
  //   std::cout<<"Ns="<<Ns<<std::endl;
  for (int i = 0; i < Ns; i++) {
    std::cout << "site " << i << ":";
    for (int d = 0; d < dim; d++) {
      std::cout << " x[" << d << "]=" << coord_PFL[i * dim + d];
    }
    std::cout << "   ";
    for (int d = 0; d < dim; d++) {
      double temp = 0;
      for (int j = 0; j < dim; j++) {
        temp += coord_PFL[i * dim + j] * vect_basis_PFS[j * dim + d];
      }
      std::cout << " x_d[" << d << "]=" << temp;
    }
    std::cout << std::endl;
  }
}

void Lattice::save_coord_PFL(char *beg_file_name, char *end_file_name) {
  std::ofstream file;
  char file_name[100];
  std::stringstream ss;
  ss << beg_file_name;
  ss >> file_name;
  strcat(file_name, end_file_name);
  file.open(file_name);
  if (!file) {
    std::cerr << "Unable to open thesave_coord_PFL file: " << std::endl;
    exit(1);
  }
  file << "#x  y" << std::endl;
  for (int i = 0; i < Ns; i++) {
    for (int d = 0; d < dim; d++) {
      file << coord_PFL[i * dim + d] << " ";
    }
    for (int d = 0; d < dim; d++) {
      double temp = 0;
      for (int jj = 0; jj < dim; jj++) {
        temp += coord_PFL[i * dim + jj] * vect_basis_PFS[jj * dim + d];
      }
      file << temp << " ";
    }
    file << "\n";
    ;
  }
  file.close();
}

void Periodic_Lattice::save_coord_RFL(char *beg_file_name,
                                      char *end_file_name) {
  std::ofstream file;
  char file_name[100];
  std::stringstream ss;
  ss << beg_file_name;
  ss >> file_name;
  strcat(file_name, end_file_name);
  file.open(file_name);
  if (!file) {
    std::cerr << "Unable to open thesave_coord_RFL file: " << std::endl;
    exit(1);
  }
  file << "#qx  qy" << std::endl;
  for (int i = 0; i < NQ; i++) {
    for (int d = 0; d < dim; d++) {
      file << coord_RFL[i * dim + d] << " ";
    }
    for (int d = 0; d < dim; d++) {
      double temp = 0;
      for (int jj = 0; jj < dim; jj++) {
        temp += coord_RFL[i * dim + jj] * vect_basis_RBS[jj * dim + d];
      }
      file << temp << " ";
    }
    file << "\n";
    ;
  }
  file.close();
}

void Lattice::print_sym(int isym) const {
  std::cout << "Display of the images of the " << isym + 1 << "th symmetry"
            << std::endl;
  for (int i = 0; i < Ns; i++) {
    std::cout << "Image of site " << i << ":" << site_sym[isym * Ns + i]
              << std::endl;
  }
}

void Lattice::print_neigh(int iJ) const {
  std::cout << "Display of the " << iJ + 1 << "th neighboring sites"
            << std::endl;
  for (int i = 0; i < Ns; i++) {
    std::cout << "  of site " << i << ":";
    for (int j = 0; j < n_neigh[iJ]; j++) {
      std::cout << " " << neigh[n_cum_neigh[iJ] + i * n_neigh[iJ] + j];
    }
    std::cout << std::endl;
  }
}

void Periodic_Lattice::print_neigh(int iJ, int im) const {
  std::cout << "Display of the " << iJ + 1 << "th neighboring sites"
            << " for the " << im << "th type of sites" << std::endl;
  int i0 = 0;
  for (int ii = 0; ii < im; ii++)
    i0 += mm[ii];
  //   std::cout<<"  i0="<<i0<<":";
  for (int i = 0; i < Nq; i++)
    for (int ii = 0; ii < mm[im]; ii++) {
      std::cout << "  of site " << i *m + i0 + ii << ":";
      for (int j = 0; j < n_neigh[iJ * nsitetype + im]; j++) {
        std::cout
            << " "
            << neigh[n_cum_neigh[iJ * nsitetype + im] +
                     (i * mm[im] + ii) * n_neigh[iJ * nsitetype + im] + j];
      }
      std::cout << std::endl;
    }
}

void Periodic_Lattice::print_properties() const {
  std::cout << "LATTICE PROPERTIES:" << std::endl;
  std::cout << "  dim=" << dim << std::endl;
  std::cout << "  m=" << m << std::endl;
  std::cout << "  Nq=" << Nq << std::endl;
  std::cout << "  Ns=" << Ns << std::endl;
  std::cout << "  NQ=" << NQ << std::endl;
  std::cout << "  Lattice size=";
  for (int i = 0; i < dim * dim; i++)
    std::cout << vect_lattice[i] << " ";
  std::cout << std::endl;
  std::cout << "  Periodicity=";
  for (int i = 0; i < dim; i++)
    std::cout << periodicity[i] << " ";
  std::cout << std::endl;
  std::cout << "  Vect_basis_RBS=";
  for (int i = 0; i < dim * dim; i++)
    std::cout << vect_basis_RBS[i] << " ";
  std::cout << std::endl;
}

void Periodic_Lattice::print_coord_PBL() const {
  std::cout << "Display of the coordinates of the sites of the Bravais "
               "physical lattice" << std::endl;
  std::cout << "Ns=" << Ns << std::endl;
  for (int i = 0; i < Ns; i++) {
    std::cout << "site " << i << ":";
    for (int d = 0; d < dim; d++) {
      std::cout << " x[" << d << "]=" << coord_PBL[i * (dim + 1) + d];
    }
    std::cout << " m=" << coord_PBL[i * (dim + 1) + dim] << "   ";

    for (int d = 0; d < dim; d++) {
      double temp = 0;
      for (int j = 0; j < dim; j++) {
        temp += coord_PBL[i * (dim + 1) + j] * vect_basis_PFS[j * dim + d];
      }
      std::cout << " x_d[" << d << "]=" << temp;
    }
    std::cout << std::endl;
  }
}

void Periodic_Lattice::print_coord_RBL() const {
  std::cout << "Display of the coordinates of the sites of the Bravais "
               "reciprocal lattice" << std::endl;
  std::cout << "Nq=" << Nq << std::endl;
  for (int i = 0; i < Nq; i++) {
    std::cout << "site " << i << ":";
    for (int d = 0; d < dim; d++) {
      std::cout << " xq[" << d << "]=" << coord_RBL[i * dim + d];
    }
    for (int d = 0; d < dim; d++) {
      double temp = 0;
      for (int j = 0; j < dim; j++) {
        temp += coord_RBL[i * dim + j] * vect_basis_RBS[j * dim + d];
      }
      std::cout << " xq_d[" << d << "]=" << temp;
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
  for (int i = 0; i < Nq; i++) {
    for (int d = 0; d < dim; d++) {
      double temp = 0;
      for (int j = 0; j < dim; j++) {
        temp += coord_RBL[i * dim + j] * vect_basis_RBS[j * dim + d];
      }
      std::cout << " " << temp;
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
  for (int i = 0; i < Nq; i++) {
    for (int d = 0; d < dim; d++) {
      std::cout << " " << coord_RBL[i * dim + d];
    }
    std::cout << std::endl;
  }
}

void Periodic_Lattice::print_coord_RFL() const {
  std::cout << "Display of the coordinates of the sites of the full reciprocal "
               "lattice" << std::endl;
  std::cout << "NQ=" << NQ << std::endl;
  for (int i = 0; i < NQ; i++) {
    std::cout << "site " << i << ":";
    for (int d = 0; d < dim; d++) {
      std::cout << " xq[" << d << "]=" << coord_RFL[i * dim + d];
    }
    for (int d = 0; d < dim; d++) {
      double temp = 0;
      for (int j = 0; j < dim; j++) {
        temp += coord_RFL[i * dim + j] * vect_basis_RBS[j * dim + d];
      }
      std::cout << " xq_d[" << d << "]=" << temp;
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;

  for (int i = 0; i < NQ; i++) {
    for (int d = 0; d < dim; d++) {
      std::cout << " " << coord_RFL[i * dim + d];
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;

  for (int i = 0; i < NQ; i++) {
    for (int d = 0; d < dim; d++) {
      double temp = 0;
      for (int j = 0; j < dim; j++) {
        temp += coord_RFL[i * dim + j] * vect_basis_RBS[j * dim + d];
      }
      std::cout << " " << temp;
    }
    std::cout << std::endl;
  }
}

int Lattice::find_site(int *coord) const {
  //   std::cout<<"Lattice::find_site dbt"<<std::endl;
  int j = 0;
  for (; j < Ns; j++) {
    int id = 0;
    for (; id < dim; id++) {
      printf("j=%d id=%d coord_PFL[j*(dim+1)+id]=%d coord[id]=%d\n", j, id,
             coord_PFL[j * dim + id], coord[id]);
      if (coord_PFL[j * dim + id] != coord[id])
        break;
    }
    if (id == dim)
      break;
  }
  if (j == Ns) {
    std::cerr << "Periodic_Lattice::find_site, no j found" << std::endl;
    exit(1);
  }
  return j;
}

int Periodic_Lattice::find_site_q(double *coord) const {
  double temp[dim];
  int iq = 0;
  bool ok = 0;
  if (dim == 1) {
    std::cerr << "find_site_q not yet implemented" << std::endl;
    exit(1);
  } else if (dim == 2) {
    temp[0] =
        (-coord[0] * vect_basis_RBS[2 + 1] + coord[1] * vect_basis_RBS[2 + 0]) /
        (vect_basis_RBS[1] * vect_basis_RBS[2 + 0] -
         vect_basis_RBS[0] * vect_basis_RBS[2 + 1]);
    temp[1] = (-coord[0] * vect_basis_RBS[1] + coord[1] * vect_basis_RBS[0]) /
              (vect_basis_RBS[2 + 1] * vect_basis_RBS[0] -
               vect_basis_RBS[2 + 0] * vect_basis_RBS[1]);
    //     std::cout<<temp[0]<<" "<<temp[1]<<"
    //     "<<(coord[0]*vect_basis_RBS[1]-coord[1]*vect_basis_RBS[0])<<"
    //     "<<(vect_basis_RBS[2+1]*vect_basis_RBS[1]-vect_basis_RBS[2+0]*vect_basis_RBS[0])<<std::endl;
    for (int i = 0; i < NQ; i++) {
      //       std::cout<<iq<<" "<<floor(temp[0]+0.1)<<"
      //       "<<floor(temp[1]+0.1)<<" "<<coord_RFL[dim*i+0]<<"
      //       "<<coord_RFL[dim*i+1]<<" "<<std::endl;
      if (floor(temp[0] + 0.1) == coord_RFL[dim * i + 0] and
          floor(temp[1] + 0.1) == coord_RFL[dim * i + 1]) {
        iq = i;
        ok = 1;
        break;
      }
    }
  } else {
    std::cerr << "find_site_q not yet implemented" << std::endl;
    exit(1);
  }
  if (ok)
    return iq;
  else {
    std::cout << "find_site_q: not found" << std::endl;
    //     exit(1);
    return 0;
  }
}

int Periodic_Lattice::find_site(int *coord) const {
  //   std::cout<<"Lattice::find_site dbt"<<std::endl;
  //   std::cout<<"m="<<m<<std::endl;
  int temp[dim];
  //   std::cerr<<"Periodic_Lattice::find_site, coord0=";
  //   for(int id=0;id<dim;id++)std::cerr<<coord[id]<<" ";std::cerr<<std::endl;
  for (int id = 0; id < dim; id++) {
    temp[id] = coord[id] % n[id];
    if (temp[id] < 0)
      temp[id] += n[id];
    coord[id] -= temp[id];
  }
  //   std::cerr<<"Periodic_Lattice::find_site, coord1=";
  //   for(int id=0;id<dim;id++)std::cerr<<coord[id]<<" ";std::cerr<<std::endl;
  int jm = 0;
  for (; jm < m; jm++) {
    int id = 0;
    for (; id < dim; id++)
      if (coord_sites_unitcell[jm * dim + id] != temp[id])
        break;
    if (id == dim)
      break;
  }
  if (jm == m) {
    std::cerr << "Periodic_Lattice::find_site, no m found, m=" << m
              << ", coord=";
    for (int id = 0; id < dim; id++)
      std::cerr << coord[id] << " ";
    std::cerr << ", temp=";
    for (int id = 0; id < dim; id++)
      std::cerr << temp[id] << " ";
    std::cerr << std::endl;
    exit(1);
  }
  if (dim == 1) {
    while (coord[0] < 0) {
      coord[0] += vect_lattice[0];
    }
    while (coord[0] >= vect_lattice[0]) {
      coord[0] -= vect_lattice[0];
    }
    return m * (coord[0] / n[0]) + jm;
  } else if (dim == 2) {
    int a = coord[0] * vect_lattice[3] - coord[1] * vect_lattice[2];
    int b = coord[1] * vect_lattice[0] - coord[0] * vect_lattice[1];
    while (a < 0) {
      coord[0] += vect_lattice[0];
      coord[1] += vect_lattice[1];
      a += NQ;
    }
    while (a >= (int)NQ) {
      coord[0] -= vect_lattice[0];
      coord[1] -= vect_lattice[1];
      a -= NQ;
    }
    while (b < 0) {
      coord[0] += vect_lattice[2];
      coord[1] += vect_lattice[3];
      b += NQ;
    }
    while (b >= (int)NQ) {
      coord[0] -= vect_lattice[2];
      coord[1] -= vect_lattice[3];
      b -= NQ;
    }
    int j = 0;
    for (; j < Ns; j += m) {
      int id = 0;
      for (; id < dim; id++)
        if (coord_PBL[j * (dim + 1) + id] != coord[id])
          break;
      if (id == dim)
        break;
    }
    if (j == Ns) {
      std::cerr << "Periodic_Lattice::find_neigh_site, no j found" << std::endl;
      exit(1);
    }
    return j + jm;
  } else {
    std::cerr
        << "Periodic_Lattice::find_neigh_site, dimension not yet implemented"
        << std::endl;
    exit(1);
  }
}

void Periodic_Lattice::fill_coord_RBL(
    double *Q) const { // in a perpendicular deformed basis
  if (dim == 1) {
    for (int i = 0; i < Nq; i++) {
      Q[2 * i] = coord_RBL[i] * vect_basis_RBS[0];
      Q[2 * i + 1] = 0;
    }
  } else if (dim == 2) {
    for (int i = 0; i < Nq; i++) {
      for (int d = 0; d < dim; d++) {
        Q[dim * i + d] = coord_RBL[i * dim + d] * fact_q;
      }
    }
  } else {
    std::cerr << "fill_coord_RBL: not yet implemented for dim=" << dim
              << std::endl;
    exit(1);
  }
}

void Periodic_Lattice::fill_coord_RFL(
    double *QQ) const { // in a perpendicular deformed basis
  if (dim == 1) {
    for (int i = 0; i < NQ; i++) {
      QQ[i * 2] = coord_RFL[i] * vect_basis_RBS[0];
      QQ[i * 2 + 1] = 0;
    }
  } else if (dim == 2) {
    for (int i = 0; i < NQ; i++) {
      for (int d = 0; d < dim; d++) {
        QQ[i * dim + d] = coord_RFL[i * dim + d] * fact_q;
      }
    }
  } else {
    std::cerr << "fill_coord_RFL: not yet implemented for dim=" << dim
              << std::endl;
    exit(1);
  }
}

inline int Periodic_Lattice::find_neigh_site_from_coord(int is,
                                                        int *coord) const {
  //   std::cout<<"is="<<is<<" ";for(int
  //   id=0;id<dim;id++)std::cout<<"coord["<<id<<"]="<<coord[id]<<" ";
  //   std::cout<<std::endl;
  int temp[dim];
  //   int temp2[dim];
  for (int id = 0; id < dim; id++) {
    temp[id] = coord_PFL[is * dim + id] + coord[id];
    //     std::cout<<" coord_PFL[is*dim+id]="<<coord_PFL[is*dim+id]<<"
    //     id="<<id<<" temp="<<temp[id]<<std::endl;
  }
  return find_site(temp);
}

void Periodic_Lattice::init_periodic_unit_cell(std::ifstream &lattice_file) {
  std::string str_temp;
  lattice_file >> dim;
  getline(lattice_file, str_temp);
  lattice_file >> nsitetype;
  getline(lattice_file, str_temp);
  mm = (int *)malloc(sizeof(int) * nsitetype);
  m = 0;
  for (int i = 0; i < nsitetype; i++) {
    lattice_file >> mm[i];
    m += mm[i];
  }
  getline(lattice_file, str_temp);

  // allocation of memory
  n = (int *)malloc(sizeof(int) * (dim * (2 + dim + m)));
  periodicity = n + dim;
  vect_lattice = periodicity + dim;
  coord_sites_unitcell = vect_lattice + dim * dim;
  vect_basis_PFS = (double *)malloc(sizeof(double) * 2 * dim * dim);
  vect_basis_RBS = vect_basis_PFS + dim * dim;

  // filling with lattice characteristics
  for (int i = 0; i < dim; i++)
    lattice_file >> n[i];
  getline(lattice_file, str_temp);
  for (int i = 0; i < m * dim; i++)
    lattice_file >> coord_sites_unitcell[i];
  getline(lattice_file, str_temp);
  for (int i = 0; i < dim * dim; i++)
    lattice_file >> vect_basis_PFS[i];
  getline(lattice_file, str_temp);
  for (int i = 0; i < dim; i++)
    periodicity[i] = 1;
  //   print_properties();
  //   std::cerr<<"fin Periodic_Lattice::init_periodic_unit_cell"<<std::endl;
}

void Periodic_Lattice::init_periodic_latt(std::ifstream &lattice_file) {
  //   std::cerr<<"dbt Periodic_Lattice::init_periodic_latt"<<std::endl;
  std::string str_temp;
  for (int i = 0; i < dim; i++)
    for (int j = 0; j < dim; j++)
      if (fmod(abs(vect_lattice[i * dim + j]), n[j]) != 0) {
        std::cerr << "Lattice size " << vect_lattice[i * dim + j]
                  << " incompatible with the type of lattice (multiples of "
                  << n[j] << " required in the " << j + 1 << "th dimension)"
                  << std::endl;
        exit(1);
      }
  if (dim == 1)
    NQ = vect_lattice[0];
  else if (dim == 2) {
    NQ = vect_lattice[0] * vect_lattice[3] - vect_lattice[1] * vect_lattice[2];
    double fact = pi2 / (NQ * (vect_basis_PFS[0] * vect_basis_PFS[3] -
                               vect_basis_PFS[1] * vect_basis_PFS[2]));
    vect_basis_RBS[0] = vect_basis_PFS[3] * fact / periodicity[0];  // v1x
    vect_basis_RBS[1] = -vect_basis_PFS[2] * fact / periodicity[0]; // v1y
    vect_basis_RBS[2] = -vect_basis_PFS[1] * fact / periodicity[1]; // v2x
    vect_basis_RBS[3] = vect_basis_PFS[0] * fact / periodicity[1];  // v2y
  } else {
    std::cerr << "Periodic_Lattice::init_periodic_perso, dimension requested "
                 "not yet implemented: maybe you can do it! dim=" << dim
              << std::endl;
    exit(1);
  }
  volume_cell_PBL = 1;
  fact_q = 1;
  for (int i = 0; i < dim; i++) {
    volume_cell_PBL *= n[i];
    fact_q *= periodicity[i];
  }
  fact_q = pi2 / (NQ * fact_q);
  if (dim == 1)
    vect_basis_RBS[0] = fact_q;
  Nq = NQ / volume_cell_PBL;
  Ns = Nq * m;

  lattice_file >> nn;
  getline(lattice_file, str_temp);
  //   std::cout<<nn<<std::endl;
  //   std::cout<<nsitetype<<std::endl;
  n_neigh = (int *)malloc(sizeof(int) * (2 * nn * nsitetype + 1));
  n_cum_neigh = n_neigh + nn * nsitetype;
  int ntemp = 0;
  std::cout << "Nq=" << Nq << std::endl;
  std::cout << "Ns=" << Ns << std::endl;
  std::cout << "NQ=" << NQ << std::endl;
  std::cout << "nn=" << nn << std::endl;
  for (int iJ = 0; iJ < nn; iJ++)
    for (int j = 0; j < nsitetype; j++) {
      lattice_file >> n_neigh[iJ * nsitetype + j];
      n_cum_neigh[iJ * nsitetype + j] = ntemp;
      ntemp += n_neigh[iJ * nsitetype + j] * Nq * mm[j];
      std::cout << "iJ=" << iJ << " j=" << j << " n_neigh[" << iJ *nsitetype + j
                << "]=" << n_neigh[iJ * nsitetype + j] << " n_cum_neigh["
                << iJ *nsitetype + j << "]=" << n_cum_neigh[iJ * nsitetype + j]
                << std::endl;
    }
  n_cum_neigh[nn * nsitetype] = ntemp;
  std::cout << "iJ=" << nn << " n_cum_neigh[" << nn *nsitetype
            << "]=" << n_cum_neigh[nn * nsitetype] << std::endl;
  getline(lattice_file, str_temp);

  // allocation of memory
  coord_RBL = (int *)malloc(
      sizeof(int) *
      (dim *
       (Nq + NQ +
        Ns * (2 * dim +
              1)))); // + n_cum_neigh[nn])+sizeof(int)*(dim*n_cum_neigh[nn]));
  coord_RFL = coord_RBL + Nq * dim;
  coord_PBL = coord_RFL + NQ * dim;
  coord_PFL = coord_PBL + Ns * (dim + 1);

  if (dim == 2) {
    int R = sqrt(sqr(vect_lattice[0] + vect_lattice[2]) +
                 sqr(vect_lattice[1] + vect_lattice[3]));
    int i = 0, j = 0;
    for (int y = -n[1] * (R / n[1]); y < (int)R; y += n[1]) {
      for (int x = 0; x < (int)R; x += n[0]) {
        int a = x * vect_lattice[3] - y * vect_lattice[2];
        int b = y * vect_lattice[0] - x * vect_lattice[1];
        if (a >= 0 and b >= 0 and a < (int)NQ and b < (int)NQ) {
          for (int k = 0; k < m; k++) {
            coord_PBL[i++] = x;
            coord_PBL[i++] = y;
            coord_PBL[i++] = k;
            coord_PFL[j++] = x + coord_sites_unitcell[2 * k];
            coord_PFL[j++] = y + coord_sites_unitcell[2 * k + 1];
          }
        }
      }
    }
    if (i != Ns * (dim + 1)) {
      std::cerr << "Periodic_Lattice::init_periodic_perso, coord_PBL missing "
                   "or too much sites, i=" << i << ", imax=" << Ns *(dim + 1)
                << std::endl;
      exit(1);
    }
    if (j != Ns * dim) {
      std::cerr << "Periodic_Lattice::init_periodic_perso, coord_PFL missing "
                   "or too much sites, j=" << j << ", jmax=" << Ns *dim
                << std::endl;
      exit(1);
    }
    i = 0, j = 0;
    int temp = periodicity[1] * periodicity[0] * NQ;
    for (int y = 0; y < periodicity[0] * periodicity[1] * NQ / n[1]; y++) {
      for (int x = 0; x < periodicity[0] * periodicity[1] * NQ / n[0]; x++) {
        int a =
            (x * vect_lattice[0] + y * vect_lattice[1] - NQ * periodicity[0]) %
            temp;
        int b =
            (x * vect_lattice[2] + y * vect_lattice[3] - NQ * periodicity[1]) %
            temp;
        if (a == 0 and b == 0) {
          coord_RBL[i++] = x;
          coord_RBL[i++] = y;
        }
      }
    }
    if (i != Nq * dim) {
      std::cerr << "Periodic_Lattice::init_periodic_perso, coord_RBL missing "
                   "or too much sites, i=" << i << ", imax=" << Nq *dim
                << std::endl;
      exit(1);
    }
    for (int iy = 0; iy < n[1]; iy++) {
      for (int ix = 0; ix < n[0]; ix++) {
        for (int i = 0; i < Nq; i++) {
          coord_RFL[2 * (ix + n[0] * iy) * Nq + 2 * i] =
              coord_RBL[2 * i] +
              periodicity[0] * periodicity[1] * ix * NQ / n[0];
          coord_RFL[2 * (ix + n[0] * iy) * Nq + 2 * i + 1] =
              coord_RBL[2 * i + 1] +
              periodicity[0] * periodicity[1] * iy * NQ / n[1];
        }
      }
    }
  } else if (dim == 1) {
    int i = 0, j = 0, p1 = (periodicity[0] == 1 ? 0 : 1);
    for (int x = 0; x < vect_lattice[0]; x += n[0]) {
      for (int k = 0; k < m; k++) {
        coord_PBL[i++] = x;
        coord_PBL[i++] = k;
        coord_PFL[j++] = x + coord_sites_unitcell[k];
      }
    }
    if (i != Ns * (dim + 1)) {
      std::cerr << "Periodic_Lattice::init_periodic_perso, coord_PBL missing "
                   "or too much sites, i=" << i << ", imax=" << Ns *(dim + 1)
                << std::endl;
      exit(1);
    }
    if (j != Ns * dim) {
      std::cerr << "Periodic_Lattice::init_periodic_perso, coord_PFL missing "
                   "or too much sites, j=" << j << ", jmax=" << Ns *dim
                << std::endl;
      exit(1);
    }
    i = 0, j = 0;
    for (int x = 0; x < NQ / n[0]; x++) {
      coord_RBL[i++] = periodicity[0] * x + p1;
      for (int ix = 0; ix < n[0]; ix++) {
        coord_RFL[j++] = periodicity[0] * (x + ix * NQ / n[0]) + p1;
      }
    }
    if (i != Nq * dim) {
      std::cerr << "Periodic_Lattice::init_periodic_perso, coord_RBL missing "
                   "or too much sites, i=" << i << ", imax=" << Nq *dim
                << std::endl;
      exit(1);
    }
    if (j != NQ * dim) {
      std::cerr << "Periodic_Lattice::init_periodic_perso, coord_RFL missing "
                   "or too much sites, j=" << j << ", jmax=" << NQ *dim
                << std::endl;
      exit(1);
    }
  } else {
    std::cerr << "Periodic_Lattice::init_periodic_perso Dimension requested "
                 "not yet implemented: you can do it! dim=" << dim << std::endl;
    exit(1);
  }

  //   std::cerr<<"deb Periodic_Lattice, neigh"<<std::endl;
  // filling of the neighbor arrays
  neigh = (int *)malloc(sizeof(int) * (3 * n_cum_neigh[nn * nsitetype]));
  Lneigh = neigh + n_cum_neigh[nn * nsitetype];
  tab_L = Lneigh + n_cum_neigh[nn * nsitetype];
  int temp[dim], count = 0;
  int Lii[Ns];
  for (int iJ = 0; iJ < nn; iJ++) {
    int isite = 0;
    int j = 0;
    while (n_neigh[iJ * nsitetype + j] == 0 and j < nsitetype)
      j++;
    //     for(int i=0;i<n_cum_neigh[nn*nsitetype];i+=2)
    //       std::cout<<"iJ="<<iJ<<", i="<<i<<", "<<tab_L[i]<<" "<<tab_L[i+1]<<"
    //       "<<std::endl;
    if (j < nsitetype) {
      int t = j + 1;
      while (n_neigh[iJ * nsitetype + t] == 0 and t < nsitetype)
        t++;
      if (t == nsitetype) {
        // both extremities of the bond are of the same type
        for (int iis = 0; iis < Ns; iis++)
          Lii[iis] = 0;
        for (int im = 0; im < mm[j];
             im++) { // loop on the sites in the unit cell of this type
          for (int in = 0; in < n_neigh[iJ * nsitetype + j];
               in++) { // loop on each link of the considered coupling from this
                       // site
            for (int id = 0; id < dim; id++) {
              lattice_file >> temp[id];
            }
            getline(lattice_file, str_temp);
            for (int is = 0; is < Nq; is++) { // loop on the unit cells
              int iS = is * m + isite + im;   // number of the considered site
              int ij = is * mm[j] + im;
              int i = find_neigh_site_from_coord(iS, temp);
              neigh[n_cum_neigh[iJ * nsitetype + j] +
                    ij * n_neigh[iJ * nsitetype + j] + in] = i;
              if (iS < i) {
                Lneigh[n_cum_neigh[iJ * nsitetype + j] +
                       ij * n_neigh[iJ * nsitetype + j] + in] = count / 2;
                // 		  std::cout<<"count="<<count<<std::endl;
                tab_L[count++] = iS;
                // 		  std::cout<<"count="<<count<<std::endl;
                tab_L[count++] = i;
                // 		std::cout<<"tab_L[i..i+1]="<<iS<<",
                // "<<i<<std::endl;
              }
              if (iS == i) {
                // 		for(int i=0;i<4;i+=2)
                // 		  std::cout<<"milieu iJ="<<iJ<<", i="<<i<<",
                // "<<tab_L[i]<<" "<<tab_L[i+1]<<" "<<std::endl;
                if (fmod(Lii[i], 2) == 0) {
                  Lneigh[n_cum_neigh[iJ * nsitetype + j] +
                         ij * n_neigh[iJ * nsitetype + j] + in] = count / 2;
                  // 		for(int i=0;i<4;i+=2)
                  // 		  std::cout<<"milieu1 iJ="<<iJ<<", i="<<i<<",
                  // "<<tab_L[i]<<" "<<tab_L[i+1]<<" "<<std::endl;
                  // 		  std::cout<<"count="<<count<<std::endl;
                  tab_L[count++] = iS;
                  // 		for(int i=0;i<4;i+=2)
                  // 		  std::cout<<"milieu2 iJ="<<iJ<<", i="<<i<<",
                  // "<<tab_L[i]<<" "<<tab_L[i+1]<<" "<<std::endl;
                  // 		  std::cout<<"count="<<count<<std::endl;
                  tab_L[count++] = i;
                  // 		for(int i=0;i<4;i+=2)
                  // 		  std::cout<<"milieu3 iJ="<<iJ<<", i="<<i<<",
                  // "<<tab_L[i]<<" "<<tab_L[i+1]<<" "<<std::endl;
                  // 		  std::cout<<"tab_L[i..i+1]="<<iS<<",
                  // "<<i<<std::endl;
                }
                Lii[i]++;
              }
            }
          }
        }
        isite += mm[j];
      } else {
        // both extremities of the bond are of different types
        for (int im = 0; im < mm[j];
             im++) { // loop on the sites in the unit cell of this type
          for (int in = 0; in < n_neigh[iJ * nsitetype + j];
               in++) { // loop on each link of the considered coupling from this
                       // site
            for (int id = 0; id < dim; id++) {
              lattice_file >> temp[id];
            }
            getline(lattice_file, str_temp);
            for (int is = 0; is < Nq; is++) { // loop on the unit cells
              int iS = is * m + isite + im;   // number of the considered site
              int iSb = is * mm[j] + im;
              int i = find_neigh_site_from_coord(iS, temp);
              neigh[n_cum_neigh[iJ * nsitetype + j] +
                    iSb * n_neigh[iJ * nsitetype + j] + in] = i;
              Lneigh[n_cum_neigh[iJ * nsitetype + j] +
                     iSb * n_neigh[iJ * nsitetype + j] + in] = count / 2;
              tab_L[count++] = iS;
              tab_L[count++] = i;
              //               std::cout<<"tab_L[i..i+1]="<<iS<<",
              //               "<<i<<std::endl;
            }
          }
        }
        for (int im = 0; im < mm[t];
             im++) { // loop on the sites in the unit cell of this type
          for (int in = 0; in < n_neigh[iJ * nsitetype + t];
               in++) { // loop on each link of the considered coupling from this
                       // site
            for (int id = 0; id < dim; id++) {
              lattice_file >> temp[id];
            }
            getline(lattice_file, str_temp);
            for (int is = 0; is < Nq; is++) { // loop on the unit cells
              int iS = is * m + im;           // number of the considered site
              for (int k = 0; k < t; k++)
                iS += mm[k];
              int iSb = is * mm[t] + im;
              int i = find_neigh_site_from_coord(iS, temp);
              neigh[n_cum_neigh[iJ * nsitetype + t] +
                    iSb * n_neigh[iJ * nsitetype + t] + in] = i;
            }
          }
        }
        isite += mm[j];
      }
    }

    //     for(int i=0;i<n_cum_neigh[nn*nsitetype];i+=2)
    //       std::cout<<"end, iJ="<<iJ<<", i="<<i<<", "<<tab_L[i]<<"
    //       "<<tab_L[i+1]<<" "<<std::endl;
  }
  if (count != n_cum_neigh[nn * nsitetype]) {
    std::cerr << "Periodic_Lattice::init_periodic_perso, tab_L elements "
                 "missing or being too much, count=" << count
              << ", n_cum_neigh[nn*nsitetype]=" << n_cum_neigh[nn * nsitetype]
              << std::endl;
    exit(1);
  }
  //   for(int i=0;i<n_cum_neigh[nn*nsitetype];i+=2)
  //     std::cout<<"i="<<i<<", "<<tab_L[i]<<" "<<tab_L[i+1]<<" "<<std::endl;

  // numerotation of bonds
  int i = 0, in;
  print_coord_PFL();
  for (int iJ = 0; iJ < nn; iJ++) {
    //     std::cout<<"iJ="<<iJ<<std::endl;
    int j = 0;
    while (n_neigh[iJ * nsitetype + j] == 0 and j < nsitetype)
      j++;
    if (j < nsitetype) { // there are bonds with coupling iJ
      //       std::cout<<"j="<<j<<std::endl;
      int t = j + 1;
      while (n_neigh[iJ * nsitetype + t] == 0 and t < nsitetype)
        t++;
      if (t == nsitetype) {
        //         std::cout<<"t="<<t<<std::endl;
        // both extremities of the bond are of the same type
        for (; i < n_cum_neigh[iJ * nsitetype + j + 1];
             i += 2) { // we browse all these links
          //           std::cout<<"i="<<i<<", tab_L[i]="<<tab_L[i]<<",
          //           tab_L[i+1]="<<tab_L[i+1]<<std::endl;
          if (tab_L[i] < tab_L[i + 1]) {
            //             std::cout<<"tab_L[i]<tab_L[i+1]"<<std::endl;
            int tab_Lb = tab_L[i + 1] + (mm[j] - m) * (tab_L[i + 1] / m);
            for (int k = 0; k < j; k++)
              tab_Lb -= mm[k];
            for (in = 0; in < n_neigh[iJ * nsitetype + j]; in++) {
              if (tab_L[i] ==
                  neigh[n_cum_neigh[iJ * nsitetype + j] +
                        tab_Lb * n_neigh[iJ * nsitetype + j] + in]) {
                Lneigh[n_cum_neigh[iJ * nsitetype + j] +
                       tab_Lb * n_neigh[iJ * nsitetype + j] + in] = i / 2;
                break;
              }
            }
            if (in == n_neigh[iJ * nsitetype + j]) {
              std::cerr << "Periodic_Lattice::init_periodic_perso, error "
                           "in==n_neigh[iJ], neighbor not found_a" << std::endl;
              exit(1);
            }
          }
        }
      } else {
        // both extremities of the bond are of different types
        for (; i < n_cum_neigh[iJ * nsitetype + j + 1]; i += 2) {
          int tab_Lb = tab_L[i + 1] + (mm[t] - m) * (tab_L[i + 1] / m);
          for (int k = 0; k < t; k++)
            tab_Lb -= mm[k];
          for (in = 0; in < n_neigh[iJ * nsitetype + t]; in++) {
            if (tab_L[i] == neigh[n_cum_neigh[iJ * nsitetype + t] +
                                  tab_Lb * n_neigh[iJ * nsitetype + t] + in]) {
              Lneigh[n_cum_neigh[iJ * nsitetype + t] +
                     tab_Lb * n_neigh[iJ * nsitetype + t] + in] = i / 2;
              break;
            }
          }
          if (in == n_neigh[iJ * nsitetype + t]) {
            std::cerr << "Periodic_Lattice::init_periodic_perso, error "
                         "in==n_neigh[iJ], neighbor not found_b" << std::endl;
            exit(1);
          }
        }
      }
    }
    std::cout << "end1" << std::endl;
  }
}

void Lattice::init_sym(std::ifstream &lattice_file) {
  std::string str_temp;
  int coord[dim];
  lattice_file >> Nsym;
  getline(lattice_file, str_temp);
  site_sym = (int *)malloc(sizeof(int) * (Ns * Nsym));
  //   print_coord_PFL();
  for (int i = 0; i < Nsym; i++) {
    int temp[dim * (dim + 1)];
    for (int id = 0; id < dim + 1; id++) {
      for (int jd = 0; jd < dim; jd++) {
        lattice_file >> temp[dim * id + jd];
      }
      getline(lattice_file, str_temp);
    }
    for (int is = 0; is < Ns; is++) {
      for (int id = 0; id < dim; id++) {
        coord[id] = temp[dim * dim + id];
        for (int jd = 0; jd < dim; jd++) {
          coord[id] += temp[dim * id + jd] * coord_PFL[is * dim + jd];
        }
      }
      site_sym[Ns * i + is] = find_site(coord);
    }
    //     print_sym(i);
  }
}

void Periodic_Lattice::init_sym(std::ifstream &lattice_file) {
  std::string str_temp;
  int coord[dim];
  lattice_file >> Nsym;
  getline(lattice_file, str_temp);
  site_sym = (int *)malloc(sizeof(int) * (Ns * Nsym));
  for (int i = 0; i < Nsym; i++) {
    int temp[dim * (dim + 1)];
    for (int id = 0; id < dim + 1; id++) {
      for (int jd = 0; jd < dim; jd++) {
        lattice_file >> temp[dim * id + jd];
      }
      getline(lattice_file, str_temp);
    }
    //     printf("\n");
    for (int is = 0; is < Ns; is++) {
      for (int id = 0; id < dim; id++) {
        coord[id] = temp[dim * dim + id];
        for (int jd = 0; jd < dim; jd++) {
          coord[id] += temp[dim * id + jd] * coord_PFL[is * dim + jd];
        }
      }
      site_sym[Ns * i + is] = find_site(coord);
    }
  }
}

int Lattice::image(int i, int isym) { return site_sym[isym * Ns + i]; }

void Lattice::init_links(const bool *if_filled, const int &nJ, const double *J,
                         int &nlinks, int *&n_links, int **&index_links,
                         int *&ij_link, double *&J_link, double *&scalar_link) {
  std::cout << "Lattice::init_links" << std::endl;
  nlinks = 0;
  for (int i = 0; i < Ns; i++) {
    n_links[i] = 0;
    int ni = 0;
    if (if_filled[i])
      for (int iJ = 0; iJ < nJ; iJ++)
        if (J[iJ])
          for (int nj = 0; nj < n_neigh[iJ]; nj++) {
            //           int j=       neigh[n_cum_neigh[iJ]+i*n_neigh[iJ]+nj];
            if (if_filled[neigh[n_cum_neigh[iJ] + i * n_neigh[iJ] + nj]])
              ni++;
          }
    nlinks += ni;
    index_links[i] = new int[2 * ni];
  }
  ij_link = new int[nlinks];
  nlinks /= 2;
  scalar_link = new double[nlinks];
  J_link = new double[nlinks];
  nlinks = 0;
  for (int i = 0; i < Ns; i++) {
    if (if_filled[i])
      for (int iJ = 0; iJ < nJ; iJ++)
        if (J[iJ])
          for (int nj = 0; nj < n_neigh[iJ]; nj++) {
            int j = neigh[n_cum_neigh[iJ] + i * n_neigh[iJ] + nj];
            if (if_filled[j] and j >= i) {
              index_links[i][2 * n_links[i]] = nlinks;
              index_links[j][2 * n_links[j]] = nlinks;
              index_links[i][2 * n_links[i] + 1] = j;
              index_links[j][2 * n_links[j] + 1] = i;
              ij_link[2 * nlinks] = i;
              ij_link[2 * nlinks + 1] = j;
              J_link[nlinks] = J[iJ];
              n_links[i]++;
              n_links[j]++;
              nlinks++;
            }
          }
  }
}

/*
void Periodic_Lattice::init_links(const bool* if_filled, const int& nJ,const
double *J, int &nlinks, int *&n_links, int **&index_links, int *&ij_link, double
*&J_link, double*&scalar_link){
  std::cout<<"Periodic_Lattice::init_links"<<std::endl;
  nlinks=0;
  for(int i=0;i<Ns;i++){
    n_links[i]=0;
    int ni=0;
    if(if_filled[i])
      for(int iJ=0;iJ<nJ;iJ++)if(J[iJ])for(int ij=0;ij<nsitetype;ij++)
        for(int nj=0;nj<n_neigh[iJ*nsitetype+ij];nj++){
//           int j=       neigh[n_cum_neigh[iJ]+i*n_neigh[iJ]+nj];
          if(if_filled[neigh[n_cum_neigh[iJ*nsitetype+ij]+i*n_neigh[iJ*nsitetype+ij]+nj]])
ni++;
          std::cout<<"n_neigh["<<iJ<<"*"<<nsitetype<<"+"<<ij<<"="<<iJ*nsitetype+ij<<"]="<<n_neigh[iJ*nsitetype+ij]<<",
iJ="<<iJ<<", "<<ni<<std::endl;
        }
    nlinks+=ni;
    index_links[i]=new int[2*ni];
  }
  ij_link=new int[nlinks];
  nlinks/=2;
  scalar_link=new double[nlinks];
  J_link=new double[nlinks];
  nlinks=0;
  for(int i=0;i<Ns;i++){
    if(if_filled[i])
      for(int iJ=0;iJ<nJ;iJ++)if(J[iJ])for(int ij=0;ij<nsitetype;ij++)
        for(int nj=0;nj<n_neigh[iJ*nsitetype+ij];nj++){
          int
j=neigh[n_cum_neigh[iJ*nsitetype+ij]+i*n_neigh[iJ*nsitetype+ij]+nj];
          if(if_filled[j] and j>=i){
            index_links[i][2*n_links[i]]=nlinks;
            index_links[j][2*n_links[j]]=nlinks;
            index_links[i][2*n_links[i]+1]=j;
            index_links[j][2*n_links[j]+1]=i;
            ij_link[2*nlinks]=i;
            ij_link[2*nlinks+1]=j;
            J_link[nlinks]=J[iJ];
            n_links[i]++;
            n_links[j]++;
            nlinks++;
          }
        }
  }
}*/

void Periodic_Lattice::init_links(const bool *if_filled, const int &nJ,
                                  const double *J, int &nlinks, int *&n_links,
                                  int **&index_links, int *&ij_link,
                                  double *&J_link, double *&scalar_link) {
  std::cout << "Periodic_Lattice::init_links" << std::endl;
  nlinks = 0;
  int i = -1;
  int Lii[Ns];
  for (int i = 0; i < Ns; i++)
    Lii[i] = 0;
  // the number of links for each site is counted.
  for (int iq = 0; iq < Nq; iq++)
    for (int in = 0; in < nsitetype; in++) {
      for (int ii = 0; ii < mm[in]; ii++) {
        //       std::cout<<"in="<<in<<", ii="<<ii<<",
        //       nlinks="<<nlinks<<std::endl;
        i++;
        n_links[i] = 0; // number of links for this site
        int ni = 0;
        if (if_filled[i])
          for (int iJ = 0; iJ < nJ; iJ++)
            if (J[iJ])
              for (int nj = 0; nj < n_neigh[iJ * nsitetype + in]; nj++) {
                int j =
                    neigh[n_cum_neigh[iJ * nsitetype + in] +
                          (iq * mm[in] + ii) * n_neigh[iJ * nsitetype + in] +
                          nj];
                if (if_filled[j]) {
                  ni++;
                }
                // 	    std::cout<<"n_neigh["<<iJ<<"*"<<nsitetype<<"+"<<in<<"="<<iJ*nsitetype+in<<"]="<<n_neigh[iJ*nsitetype+in]<<",
                // iJ="<<iJ<<", "<<ni<<std::endl;
              }
        nlinks += ni;
        index_links[i] = new int[2 * ni];
      }
    }
  //   //the number of links for eqch site is counted.
  //   for(int iq=0;iq<Nq;iq++)for(int in=0;in<nsitetype;in++){
  //     for(int ii=0;ii<mm[in];ii++){
  //       std::cout<<"in="<<in<<", ii="<<ii<<", nlinks="<<nlinks<<std::endl;
  //       i++;
  //       n_links[i]=0;
  //       int ni=0;
  //       if(if_filled[i])
  // 	for(int iJ=0;iJ<nJ;iJ++)if(J[iJ])
  // 	  for(int nj=0;nj<n_neigh[iJ*nsitetype+in];nj++){
  //             int j=
  //             neigh[n_cum_neigh[iJ*nsitetype+in]+(iq*mm[in]+ii)*n_neigh[iJ*nsitetype+in]+nj];
  // 	    if(if_filled[j]){
  // 	      if(i==j){
  // 		Lii[i]++;
  // 		if(fmod(Lii[i],2)==1)ni++;
  // 	      }
  // 	    }
  // 	    std::cout<<"n_neigh["<<iJ<<"*"<<nsitetype<<"+"<<in<<"="<<iJ*nsitetype+in<<"]="<<n_neigh[iJ*nsitetype+in]<<",
  // iJ="<<iJ<<", "<<ni<<std::endl;
  // 	  }
  //       nlinks+=ni;
  //       index_links[i]=new int[2*ni];
  //     }
  //   }
  ij_link = new int[nlinks];
  //   std::cout<<"nlinks="<<nlinks<<std::endl;
  nlinks /= 2;
  scalar_link = new double[nlinks];
  J_link = new double[nlinks];
  nlinks = 0;
  i = -1;
  for (int i = 0; i < Ns; i++)
    Lii[i] = 0;
  for (int iq = 0; iq < Nq; iq++)
    for (int in = 0; in < nsitetype; in++)
      for (int ii = 0; ii < mm[in]; ii++) {
        i++;
        if (if_filled[i])
          for (int iJ = 0; iJ < nJ; iJ++)
            if (J[iJ])
              for (int nj = 0; nj < n_neigh[iJ * nsitetype + in]; nj++) {
                int j =
                    neigh[n_cum_neigh[iJ * nsitetype + in] +
                          (iq * mm[in] + ii) * n_neigh[iJ * nsitetype + in] +
                          nj];
                if (if_filled[j] and j > i) {
                  index_links[i][2 * n_links[i]] = nlinks;
                  index_links[j][2 * n_links[j]] = nlinks;
                  index_links[i][2 * n_links[i] + 1] = j;
                  index_links[j][2 * n_links[j] + 1] = i;
                  ij_link[2 * nlinks] = i;
                  ij_link[2 * nlinks + 1] = j;
                  J_link[nlinks] = J[iJ];
                  n_links[i]++;
                  n_links[j]++;
                  nlinks++;
                }
                if (if_filled[j] and j == i) {
                  Lii[i]++;
                  if (fmod(Lii[i], 2) == 1) {
                    index_links[i][2 * n_links[i]] = nlinks;
                    // 	      index_links[j][2*n_links[j]]=nlinks;
                    index_links[i][2 * n_links[i] + 1] = j;
                    // 	      index_links[j][2*n_links[j]+1]=i;
                    ij_link[2 * nlinks] = i;
                    // 	      ij_link[2*nlinks+1]=j;
                    J_link[nlinks] = J[iJ];
                    // 	      n_links[i]++;
                    n_links[j]++;
                    nlinks++;
                  }
                }
                //           std::cout<<"iJ="<<iJ<<",
                //           nlinks="<<nlinks<<std::endl;
              }
      }
}

// modified, to throw !
// void Periodic_Lattice::init_links(const bool* if_filled, const int& nJ,const
// double *J, int &nlinks, int *&n_links, int **&index_links, int *&ij_link,
// double *&J_link, double*&scalar_link){
//   std::cout<<"Periodic_Lattice::init_links"<<std::endl;
//   nlinks=0;
//   for(int i=0;i<Ns;i++)n_links[i]=0;
//   for(int iJ=0;iJ<nJ;iJ++)if(J[iJ]){//loop on the type of J
//     for(int ij=0;ij<nsitetype;ij++){
//       for(int i=0;i<Nq;i++){//loop on the unit cells
// 	int iS=i*m;
// 	int iSb=i*mm[ij];
// 	for(int k=0;k<ij;k++)iS+=mm[k];
// 	for(int k=0;k<mm[ij];k++){
// 	  iS++;             //site number
// 	  if(if_filled[iS]){
// 	    iSb++;
// 	    int ni=0;
// 	    if(if_filled[iS])
// 	      for(int nj=0;nj<n_neigh[iJ*nsitetype+ij];nj++){
// 		if(if_filled[neigh[n_cum_neigh[iJ*nsitetype+ij]+iSb*n_neigh[iJ*nsitetype+ij]+nj]])ni++;
//   //
//   std::cout<<"n_neigh["<<iJ<<"*"<<nsitetype<<"+"<<ij<<"="<<iJ*nsitetype+ij<<"]="<<n_neigh[iJ*nsitetype+ij]<<",
//   iJ="<<iJ<<", "<<ni<<std::endl;
// 	      }
// 	      nlinks+=ni;
// 	      n_links[iS]+=ni;
// 	   }
//          }
//       }
//     }
//   }
//   for(int i=0;i<Ns;i++)index_links[i]=new int[2*n_links[i]];
//   ij_link=new int[nlinks];
//   nlinks/=2;
//   scalar_link=new double[nlinks];
//   J_link=new double[nlinks];
//   nlinks=0;
//   for(int iJ=0;iJ<nJ;iJ++)if(J[iJ]){//loop on the type of J
//     for(int ij=0;ij<nsitetype;ij++){
//       for(int i=0;i<Nq;i++){//loop on the unit cells
// 	int iS=i*m;
// 	int iSb=i*mm[ij];
// 	for(int k=0;k<ij;k++)iS+=mm[k];
// 	for(int k=0;k<mm[ij];k++){
// 	  iS++;             //site number
// 	  if(if_filled[iS]){
// 	    for(int nj=0;nj<n_neigh[iJ*nsitetype+ij];nj++){
//               int
//               j=neigh[n_cum_neigh[iJ*nsitetype+ij]+iSb*n_neigh[iJ*nsitetype+ij]+nj];
// 	      if(if_filled[j] and j>=iS){
// 		index_links[iS][2*n_links[iS]]=nlinks;
// 		index_links[j][2*n_links[j]]=nlinks;
// 		index_links[iS][2*n_links[iS]+1]=j;
// 		index_links[j][2*n_links[j]+1]=i;
// 		ij_link[2*nlinks]=i;
// 		ij_link[2*nlinks+1]=j;
// 		J_link[nlinks]=J[iJ];
// 		n_links[i]++;
// 		n_links[j]++;
// 		nlinks++;
// 	      }
// 	    }
// 	  }
// 	}
//       }
//     }
//   }
// }

int Lattice::correlations_neigh_vpvm(int iJ, int *spin) {
  int corr = 0;
  for (int i = 0; i < Ns; i++) {
    for (int j = 0; j < n_neigh[iJ]; j++) {
      if (spin[i] != spin[neigh[n_cum_neigh[iJ] + i * n_neigh[iJ] + j]])
        corr++;
    }
  }
  return Ns * n_neigh[iJ] - 2 * corr;
  //   for(int i=0;i<1;i++){
  //     for(int j=0;j<1;j++){
  //       if(spin[i]!=spin[first_neigh[i*n_first_neigh+j]])corr++;
  //     }
  //   }
  //   return 1-2*corr;
}

void Periodic_Lattice::compute_Sq(double *fx, double *&Sq) {
  // The results are not invariant by rotation of theta if the lattice is not
  // invariant by a rotation of theta around any site. It is because we compute
  // the Fourier transform of the correlations between all sites and a single
  // site.
  Sq = new double[NQ];
  for (int iq = 0; iq < NQ; iq++) {
    Sq[iq] = 0;
    for (int ir = 0; ir < Ns; ir++) {
      int qr = 0;
      for (int d = 0; d < dim; d++) {
        qr +=
            (coord_PFL[ir * dim + d] - coord_PFL[d]) * coord_RFL[iq * dim + d];
      }
      Sq[iq] += cos(fact_q * qr) * fx[ir];
    }
    Sq[iq] /= NQ;
  }
}

double Periodic_Lattice::compute_Sq(double *fx, int iq) const {
  double Sq = 0;
  for (int ir = 0; ir < Ns; ir++) {
    int qr = 0;
    for (int d = 0; d < dim; d++) {
      qr += (coord_PFL[ir * dim + d] - coord_PFL[d]) * coord_RFL[iq * dim + d];
    }
    Sq += cos(fact_q * qr) * fx[ir];
  }
  return Sq / NQ;
}

double Periodic_Lattice::verif_sumrule(double *Sq) const {
  double sumr = 0;
  for (int iq = 0; iq < NQ; iq++) {
    sumr += Sq[iq];
  }
  return sumr;
}

void Periodic_Lattice::compute_Sq_powder(double *&Qp, double *&Sqp, double *Sq,
                                         int ntheta, int nqp) {
  Qp = (double *)malloc(nqp * sizeof(double));
  Sqp = (double *)malloc(nqp * sizeof(double));
  double qmax = pi2 * 2;
  double dQ = (double)(qmax) / (nqp - 1);
  for (int i = 0; i < nqp; i++)
    Qp[i] = i * dQ;
  double *sint = (double *)malloc(ntheta * sizeof(double));
  double dtheta = pi / (2 * ntheta);
  for (int i = 0; i < ntheta; i++)
    sint[i] = sin(i * dtheta);
  double *SQtheta = (double *)malloc(nqp * sizeof(double));
  memset(SQtheta, 0, nqp * sizeof(double));
  for (int iq = 0; iq < NQ; iq++) {
    double *q = new double[dim];
    for (int d = 0; d < dim; d++) {
      q[d] = 0;
      for (int d1 = 0; d1 < dim; d1++) {
        q[d] += coord_RFL[iq * dim + d1] * vect_basis_RBS[d1 * dim + d];
      }
    }
    if (dim == 2) {
      for (int ix = -2; ix < 1; ix++)
        for (int iy = -2; iy < 1; iy++) { // to replicate Sq
          double normq = sqrt(
              (double)(sqr(q[0] +
                           NQ * (ix * periodicity[0] * vect_basis_RBS[0] +
                                 iy * periodicity[1] * vect_basis_RBS[2])) +
                       sqr(q[1] +
                           NQ * (ix * periodicity[0] * vect_basis_RBS[1] +
                                 iy * periodicity[1] * vect_basis_RBS[3]))));
          int ik = int(normq / dQ + 0.5);
          if (ik < nqp && ik > 0) {
            SQtheta[ik] += Sq[iq] / normq;
          }
        }
    } else if (dim == 1) {
      for (int ix = -2; ix < 1; ix++) { // to replicate Sq
        double normq =
            NQ * abs(q[0] + (ix * periodicity[0] * vect_basis_RBS[0]));
        int ik = int(normq / dQ + 0.5);
        if (ik < nqp && ik > 0) {
          SQtheta[ik] += Sq[iq] / normq;
        }
      }
    } else {
      std::cout << "Periodic_Lattice::compute_Sq_powder dimension not yet "
                   "implemented: dim=" << dim << std::endl;
      exit(1);
    }
  }
  for (int iq = 0; iq < nqp; iq++) {
    double ssq = 0, Q = Qp[iq];
    int iQsint;
    for (int it = 1; it < ntheta; it++) {
      iQsint = int(Q * sint[it] / dQ);
      ssq += sint[it] * SQtheta[iQsint];
    }
    ssq -= sint[ntheta - 1] * SQtheta[iQsint] / 2;
    Sqp[iq] = ssq * dtheta;
  }
  free(SQtheta);
  free(sint);
}

void Lattice::save_Sx(char *beg_file_name, char *end_file_name, double *Sx) {
  std::ofstream SX_file;
  char SX_file_name[100];
  std::stringstream ss;
  ss << beg_file_name;
  ss >> SX_file_name;
  strcat(SX_file_name, end_file_name);
  SX_file.open(SX_file_name);
  if (!SX_file) {
    std::cerr << "Unable to open the SQ file: "
              << "SQ_register.res" << std::endl;
    exit(1);
  }
  SX_file << "#Qx  Qy  S(Q)" << std::endl;
  for (int ix = 0; ix < Ns; ix++) {
    for (int d = 0; d < dim; d++) {
      double x = 0;
      for (int d1 = 0; d1 < dim; d1++)
        x += (double)coord_PFL[ix * dim + d1] * vect_basis_PFS[d1 * dim + d];
      SX_file << x << " ";
    }
    SX_file << Sx[ix] << std::endl;
  }
  SX_file.close();
}

void Lattice::save_Sx(char *beg_file_name, char *end_file_name, int *Sx) {
  std::ofstream SX_file;
  char SX_file_name[100];
  std::stringstream ss;
  ss << beg_file_name;
  ss >> SX_file_name;
  strcat(SX_file_name, end_file_name);
  SX_file.open(SX_file_name);
  if (!SX_file) {
    std::cerr << "Unable to open the SQ file: "
              << "SQ_register.res" << std::endl;
    exit(1);
  }
  SX_file << "#Qx  Qy  S(Q)" << std::endl;
  for (int ix = 0; ix < Ns; ix++) {
    for (int d = 0; d < dim; d++) {
      double x = 0;
      for (int d1 = 0; d1 < dim; d1++)
        x += (double)coord_PFL[ix * dim + d1] * vect_basis_PFS[d1 * dim + d];
      SX_file << x << " ";
    }
    SX_file << Sx[ix] << std::endl;
  }
  SX_file.close();
}

void Periodic_Lattice::save_Sq(char *beg_file_name, char *end_file_name,
                               double *Sq) {
  std::ofstream SQ_file;
  char SQ_file_name[100];
  std::stringstream ss;
  ss << beg_file_name;
  ss >> SQ_file_name;
  strcat(SQ_file_name, end_file_name);
  //   std::cout<<"#Qx  Qy  S(Q)"<<std::endl;
  SQ_file.open(SQ_file_name);
  //   std::cout<<"#Qx  Qy  S(Q)"<<std::endl;
  if (!SQ_file) {
    std::cerr << "Unable to open the SQ file: "
              << "SQ_register.res" << std::endl;
    exit(1);
  }
  //   std::cout<<"#Qx  Qy  S(Q)"<<std::endl;
  SQ_file << "#Qx  Qy  S(Q)" << std::endl;
  for (int iq = 0; iq < NQ; iq++) {
    double q;
    for (int d = 0; d < dim; d++) {
      q = 0;
      for (int d1 = 0; d1 < dim; d1++)
        q += (double)coord_RFL[iq * dim + d1] * vect_basis_RBS[d1 * dim + d];
      SQ_file << q << " ";
    }
    SQ_file << Sq[iq] << std::endl;
  }
  //   std::cout<<"#Qx  Qy  S(Q)"<<std::endl;
  SQ_file.close();
}

void Periodic_Lattice::save_Sq_powder(char *beg_file_name, char *end_file_name,
                                      int nqp, double *Qp, double *Sq) {
  std::ofstream SQpowder_file;
  char SQpowder_file_name[100];
  std::stringstream ss;
  ss << beg_file_name;
  ss >> SQpowder_file_name;
  strcat(SQpowder_file_name, end_file_name);
  SQpowder_file.open(SQpowder_file_name);
  if (!SQpowder_file) {
    std::cerr << "Unable to open the SQpowder file: "
              << "SQpowder_register.res" << std::endl;
    exit(1);
  }
  SQpowder_file << "#|Q|  SQpowder(Q)" << std::endl;
  for (int iq = 0; iq < nqp; iq++) {
    SQpowder_file << Qp[iq] << " " << Sq[iq] << std::endl;
  }
  SQpowder_file.close();
}
