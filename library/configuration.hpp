#ifndef _CONFIGURATION_
#define _CONFIGURATION_

#include <cstring>
#include <sstream>
#include "Wang_Landau.hpp"
#include "constant.hpp"
#include "lattice.hpp"
#include "random.hpp"

// structure spin
template <int d_spin = 3> struct spin { // d_spin=3 is the value by default.
public:
  double s[d_spin];
  void aleat();
  void print();
  void charac(char *&c);
};

// structure Spin_configuration
template <int d_spin = 3> struct Spin_configuration {
protected:
  int Ns, nspins, nlinks; // number of sites and of spins and of links
  spin<d_spin> *spins;    // orientation of the spins
  bool *if_filled;        // 0 if the site is empty
  double *J_link;         // gives the value of the coupling for each link
  double *scalar_link;    // scalar product of two spins of a link
  int *n_links;           // number of links of a site
  int **index_links; // index_links[i][2*j] : number of the jth link associated
                     // to site i
  // index_links[i][2*j+1] : number of the site of the jth link associated to
  // site i
  int *ij_link; // ij_link[2*ilink]=i, ij_link[2*ilink+1]=j
  int *i_spins; // number of the occupied sites
  double beta;
  double T;
  double E;
  void init_energie();

public:
  Spin_configuration();
  Spin_configuration(int Ns0);
  ~Spin_configuration();
  void init(int &nJ, double *J, double &T, double filling,
            std::string initial_conditions, Lattice *lattice);
  //   void init(int &nJ,double *J,double &T,Lattice* lattice,double
  //   filling=1,std::string initial_conditions="random");
  void clean();
  //   unsigned int get_Ns() const;
  double get_E() const;
  void print(bool b = true);
  void init_WL(Wang_Landau_histograms *wl);
  void WL_step(Wang_Landau_histograms *wl);
  void MC_step();
  void update_corr(double *Sx, int i0 = 0);
  void save(char *begin_file_name, char *end_file_name);
};

// printing of a spin
template <int d_spin> void spin<d_spin>::print() {
  for (int i = 0; i < d_spin; i++) {
    std::cout << s[i] << " ";
  }
  std::cout << std::endl;
}

// //creation of a random spin uniformly distributed
// template<> void spin<3>::aleat(){
//   double phi=pi2*generate_double();
//   s[2]=2*generate_double()-1.;
//   double rtemp=sqrt(1.-s[2]*s[2]);
//   s[0]=rtemp*cos(phi);
//   s[1]=rtemp*sin(phi);
// }
//
// template<> void spin<2>::aleat(){
//   double phi=pi2*generate_double();
//   s[0]=cos(phi);
//   s[1]=sin(phi);
// }
//
// template<> void spin<1>::aleat(){
//   s[0]=2*((double)generate_int(2))-1;
// }

// addition of two spins
template <int d_spin> spin<d_spin> add_spin(spin<d_spin> S1, spin<d_spin> S2) {
  spin<d_spin> Stemp;
  for (int i = 0; i < d_spin; i++) {
    Stemp.s[i] = S1.s[i] + S2.s[i];
  }
  return (Stemp);
}

// substraction of two spins
template <int d_spin> spin<d_spin> sub_spin(spin<d_spin> S1, spin<d_spin> S2) {
  spin<d_spin> Stemp;
  for (int i = 0; i < d_spin; i++) {
    Stemp.s[i] = S1.s[i] - S2.s[i];
  }
  return (Stemp);
}

// scalar product of two spins
template <int d_spin> double scalar_spin(spin<d_spin> S1, spin<d_spin> S2) {
  double r = 0;
  for (int i = 0; i < d_spin; i++) {
    r += S1.s[i] * S2.s[i];
  }
  return (r);
}

// CONSTRUCTORS of Spin_configuration
template <int d_spin>
Spin_configuration<d_spin>::Spin_configuration(int Ns0)
    : Ns(Ns0), nspins(0), E(0), T(0), beta(0), J_link(0), scalar_link(0),
      ij_link(0), i_spins(0) {
  spins = new spin<d_spin>[Ns]; // orientation of the spin
  if_filled = new bool[Ns];     // 0 if the site is empty
  index_links = new int *[Ns];
  n_links = new int[Ns];
}

template <int d_spin>
Spin_configuration<d_spin>::Spin_configuration()
    : Ns(0), spins(0), nspins(0), E(0), T(0), beta(0), if_filled(0), J_link(0),
      scalar_link(0), n_links(0), index_links(0), nlinks(0), ij_link(0),
      i_spins(0) {}

template <int d_spin>
void Spin_configuration<d_spin>::init(int &nJ, double *J, double &T0,
                                      double filling,
                                      std::string initial_conditions,
                                      Lattice *latt) {
  T = T0;
  if (T > 0)
    beta = 1 / T;
  else {
    std::cerr << "T=0... not yet implemented" << std::endl;
    exit(1);
  }
  nspins = 0;
  nlinks = 0;
  if_filled[0] = true;
  nspins++;
  double f = (Ns * filling - 1) / (Ns - 1);
  if (initial_conditions == "random") {
    spins[0].aleat();
    for (int i = 1; i < Ns; i++) {
      if_filled[i] = (generate_double() < f) ? true : false;
      if (if_filled[i]) {
        nspins++;
        spins[i].aleat();
      }
    }
  } else if (initial_conditions == "uniform") {
    spins[0].s[0] = 1;
    for (int x = 1; x < d_spin; x++)
      spins[0].s[x] = 0;
    for (int i = 1; i < Ns; i++) {
      if_filled[i] = (generate_double() < f) ? true : false;
      if (if_filled[i]) {
        nspins++;
        spins[i].s[0] = 1;
        for (int x = 1; x < d_spin; x++)
          spins[i].s[x] = 0;
      }
    }
  } else if (initial_conditions == "bidirectionnal") {
    spins[0].s[0] = 1;
    for (int x = 1; x < d_spin; x++)
      spins[0].s[x] = 0;
    for (int i = 2; i < Ns; i += 2) {
      if_filled[i] = (generate_double() < f) ? true : false;
      if (if_filled[i]) {
        nspins++;
        spins[i].s[0] = 1;
        for (int x = 1; x < d_spin; x++)
          spins[0].s[x] = 0;
      }
    }
    for (int i = 1; i < Ns; i += 2) {
      if_filled[i] = (generate_double() < f) ? true : false;
      if (if_filled[i]) {
        nspins++;
        spins[i].s[0] = -1;
        for (int x = 1; x < d_spin; x++)
          spins[0].s[x] = 0;
      }
    }
  } else if (initial_conditions == "tridirectionnal") {
    if (d_spin < 3) {
      std::cerr << "initial condition (tridirectionnal) incompatible with the "
                   "spin dimension : d_spins=" << d_spin << std::endl;
      exit(1);
    }
    spins[0].s[0] = 1;
    for (int x = 1; x < d_spin; x++)
      spins[0].s[x] = 0;
    for (int i = 3; i < Ns; i += 3) {
      if_filled[i] = (generate_double() < f) ? true : false;
      if (if_filled[i]) {
        nspins++;
        spins[i].s[0] = 1;
        for (int x = 1; x < d_spin; x++)
          spins[0].s[x] = 0;
      }
    }
    for (int i = 1; i < Ns; i += 3) {
      if_filled[i] = (generate_double() < f) ? true : false;
      if (if_filled[i]) {
        nspins++;
        spins[i].s[0] = -0.5;
        spins[i].s[1] = sqrt(3 / 2);
        for (int x = 2; x < d_spin; x++)
          spins[0].s[x] = 0;
      }
    }
    for (int i = 2; i < Ns; i += 3) {
      if_filled[i] = (generate_double() < f) ? true : false;
      if (if_filled[i]) {
        nspins++;
        spins[i].s[0] = -0.5;
        spins[i].s[1] = -sqrt(3 / 2);
        for (int x = 2; x < d_spin; x++)
          spins[0].s[x] = 0;
      }
    }
  } else {
    std::cout << "initial conditions not defined" << std::endl;
    exit(1);
  }
  i_spins = new int[nspins];
  nspins = 0;
  for (int i = 0; i < Ns; i++) {
    if (if_filled[i]) {
      i_spins[nspins] = i;
      nspins++;
    }
  }
  latt->init_links(if_filled, nJ, J, nlinks, n_links, index_links, ij_link,
                   J_link, scalar_link);
  init_energie();
}

template <int d_spin> void Spin_configuration<d_spin>::clean() {
  if (i_spins) {
    delete[] i_spins;
    i_spins = 0;
  }
  if (ij_link) {
    delete[] ij_link;
    ij_link = 0;
  }
  if (scalar_link) {
    delete[] scalar_link;
    scalar_link = 0;
  }
  if (J_link) {
    delete[] J_link;
    J_link = 0;
  }
}

// DESTRUCTOR of Spin_configuration
template <int d_spin> Spin_configuration<d_spin>::~Spin_configuration() {
  //   if(i_spins)delete[] i_spins;
  //   if(ij_link)delete[] ij_link;
  //   if(scalar_link)delete[] scalar_link;
  //   if(J_link)delete[] J_link;
}

template <int d_spin> double Spin_configuration<d_spin>::get_E() const {
  return E;
}

// print all informations on a configuration
template <int d_spin> void Spin_configuration<d_spin>::print(bool b) {
  std::cout << "number of sites Ns=" << Ns
            << ", number of spins nspins=" << nspins
            << ", number of links nlinks=" << nlinks << std::endl;
  if (b) {
    for (int i = 0; i < Ns; i++) {
      std::cout << "spin[" << i << "]";
      if (if_filled[i]) {
        std::cout << ": ";
        spins[i].print();
      } else
        std::cout << std::endl;
      std::cout << sqr(spins[i].s[0]) + sqr(spins[i].s[1]) + sqr(spins[i].s[2])
                << std::endl;
    }
  }
}

template <int d_spin> void Spin_configuration<d_spin>::init_energie() {
  E = 0;
  for (int i = 0; i < nlinks; i++) {
    scalar_link[i] =
        scalar_spin(spins[ij_link[2 * i]], spins[ij_link[2 * i + 1]]);
    E += J_link[i] * scalar_link[i];
  }
}

template <int d_spin> void Spin_configuration<d_spin>::MC_step() {
  for (int n = 0; n < nspins; n++) {
    int i = i_spins[gsl_rng_uniform_int(rng, nspins)];
    spin<d_spin> S_new;
    S_new.aleat();
    double deltaE = 0;
    double temp[n_links[i]];
    for (int j = 0; j < n_links[i]; j++) {
      temp[j] = scalar_spin(S_new, spins[index_links[i][2 * j + 1]]);
      deltaE += J_link[index_links[i][2 * j]] *
                (temp[j] - scalar_link[index_links[i][2 * j]]);
    }
    if (generate_double() < exp(-deltaE * beta)) {
      E += deltaE;
      spins[i] = S_new;
      for (int j = 0; j < n_links[i]; j++) {
        scalar_link[index_links[i][2 * j]] = temp[j];
      }
    }
  }
}

template <int d_spin>
void Spin_configuration<d_spin>::init_WL(Wang_Landau_histograms *wl) {
  wl->raz_histo_count();
  int count = 0;
  while (wl->E_not_in_range(E) and count < 10000) {
    count++;
    int i = i_spins[gsl_rng_uniform_int(rng, nspins)];
    spin<d_spin> S_new;
    S_new.aleat();
    double deltaE = 0;
    double temp[n_links[i]];
    for (int j = 0; j < n_links[i]; j++) {
      temp[j] = scalar_spin(S_new, spins[index_links[i][2 * j + 1]]);
      deltaE += J_link[index_links[i][2 * j]] *
                (temp[j] - scalar_link[index_links[i][2 * j]]);
    }
    if (wl->is_better(E, deltaE)) {
      E += deltaE;
      spins[i] = S_new;
      for (int j = 0; j < n_links[i]; j++) {
        scalar_link[index_links[i][2 * j]] = temp[j];
      }
    }
  }
  if (count == 10000) {
    std::cout << "Maximal number of initiation iteration attained. Are Emin "
                 "and Emax ok ???" << std::endl;
    exit(1);
  }
  wl->update_iE(E);
}

template <int d_spin>
void Spin_configuration<d_spin>::WL_step(Wang_Landau_histograms *wl) {
  for (int imc = 0; imc < Ns; imc++) {
    int i = i_spins[gsl_rng_uniform_int(rng, nspins)];
    spin<d_spin> S_new;
    S_new.aleat();
    double deltaE = 0;
    double temp[n_links[i]];
    for (int j = 0; j < n_links[i]; j++) {
      temp[j] = scalar_spin(S_new, spins[index_links[i][2 * j + 1]]);
      deltaE += J_link[index_links[i][2 * j]] *
                (temp[j] - scalar_link[index_links[i][2 * j]]);
    }
    if (wl->if_accept(E + deltaE)) {
      E += deltaE;
      spins[i] = S_new;
      for (int j = 0; j < n_links[i]; j++) {
        scalar_link[index_links[i][2 * j]] = temp[j];
      }
    }
  }
}

template <int d_spin>
void Spin_configuration<d_spin>::update_corr(double *Sx, int i0) {
  Sx[0]++;
  for (int i = 1; i < Ns; i++) {
    if (if_filled[i]) {
      Sx[i0 + i] += scalar_spin(spins[0], spins[i]);
    }
  }
}

template <int d_spin>
void Spin_configuration<d_spin>::save(char *begin_file_name,
                                      char *end_file_name) {
  std::ofstream config_file;
  char config_file_name[100];
  std::stringstream ss;
  ss << begin_file_name;
  ss >> config_file_name;
  strcat(config_file_name, end_file_name);
  config_file.open(config_file_name);
  if (!config_file) {
    std::cerr << "Unable to open the config file: "
              << "config_register.res" << std::endl;
    exit(1);
  }
  config_file << "#i S(i)" << std::endl;
  for (int i = 0; i < nspins; i++) {
    char *c;
    spins[i_spins[i]].charac(c);
    config_file << i_spins[i] << " " << c << std::endl;
  }
  config_file.close();
}

template <int d_spin> void spin<d_spin>::charac(char *&c) {
  c = new char[100];
  char ctemp[10];
  sprintf(c, " ");
  for (int d = 0; d < d_spin; d++) {
    sprintf(ctemp, "%4.3f ", s[d]);
    strcat(c, ctemp);
  }
}

#endif
