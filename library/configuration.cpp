#include "configuration.hpp"

gsl_rng *rng;

// creation of a random spin uniformly distributed
template <> void spin<3>::aleat() {
  double phi = pi2 * generate_double();
  s[2] = 2 * generate_double() - 1.;
  double rtemp = sqrt(1. - s[2] * s[2]);
  s[0] = rtemp * cos(phi);
  s[1] = rtemp * sin(phi);
}

template <> void spin<2>::aleat() {
  double phi = pi2 * generate_double();
  s[0] = cos(phi);
  s[1] = sin(phi);
}

template <> void spin<1>::aleat() { s[0] = 2 * ((double)generate_int(2)) - 1; }
