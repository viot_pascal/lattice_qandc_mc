#ifndef _RANDOM_
#define _RANDOM_
#include "gsl/gsl_rng.h"
// #include </exports/cluster/Download/gsl-1.14/gsl/gsl_rng.h>

// if used, need in the Cmake file:
// find_package(GSL REQUIRED)
//#says that configuration requires the GSL and WL libraries
// target_link_libraries(your_cpp_file ${GSL_LIBRARIES})
extern gsl_rng *rng; // need to be defined somewhere else

inline void init_random(int seed) {
  rng = gsl_rng_alloc(gsl_rng_default);
  gsl_rng_set(rng, seed);
}
inline bool generate_bool() { return gsl_rng_uniform(rng) > 0.5; }
inline int generate_int(int n) { return gsl_rng_uniform_int(rng, n); }
inline double generate_double() { return gsl_rng_uniform(rng); }

#endif
