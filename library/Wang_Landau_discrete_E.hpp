#ifndef _WL_
#define _WL_

#include "constant.hpp"
#include "random.hpp"

struct Wang_Landau_histograms {
protected:
  int *histo_count, iE, nE, ndone;
  double f, tolerance;
  int dE, Emin, Emax;
  double *ln_density;
  inline int find_iE(int E);

public:
  Wang_Landau_histograms();
  Wang_Landau_histograms(int dE, int Emin0, int Emax0, double f0);
  ~Wang_Landau_histograms();
  inline const double get_f();
  inline const int get_nE();
  inline const int get_iE();
  inline void update_f();
  inline void raz_histo_count();
  inline bool if_accept(int Enew);
  inline const bool histo_is_not_flat();
  inline void update_iE(int E);
  inline const bool E_not_in_range(int const E);
  inline const bool is_better(int const E, int const deltaE);
  void print_histo();
  void renormalize_corr(double *Sx, int const Ns);
  void print_all_E();
  void verify_dE(int deltaE);
  void adapt_E_range(int E);
  void read_lngE(char *file_name);
  void save_Sx(double *Sx, const int Ns, char *beg_file_name,
               char *end_file_name);
  void save_lngE(char *beg_file_name, char *end_file_name);
  void save_obs(double *m, char *beg_file_name, char *end_file_name);
  void save_obs(double *m, double *m1, double *m2, char *beg_file_name, char *end_file_name);
};

const int Wang_Landau_histograms::get_nE() { return nE; }

const int Wang_Landau_histograms::get_iE() { return iE; }

const double Wang_Landau_histograms::get_f() { return f; }

void Wang_Landau_histograms::update_f() { f /= 2; }

void Wang_Landau_histograms::raz_histo_count() {
  ndone = 0;
  for (int i = 0; i < nE; i++)
    histo_count[i] = 0;
  double min = ln_density[0];
  for (int i = 1; i < nE; i++)
    if (ln_density[i] < min)
      min = ln_density[i];
  for (int i = 0; i < nE; i++)
    ln_density[i] -= min;
}

const bool Wang_Landau_histograms::histo_is_not_flat() {
  if (ndone == 0) {
    return true; // then we continue the WL algorithm
  }
  double fact = tolerance * ndone / nE;
  for (int i = 0; i < nE; i++) {
    if ((double)histo_count[i] < fact)
      return true;
  }
  return false;
}

bool Wang_Landau_histograms::if_accept(int Enew) {
  int iEnew = find_iE(Enew);
  ndone++;
  bool res = true;
  if (iEnew < 0 or iEnew >= nE or
      generate_double() > exp(ln_density[iE] - ln_density[iEnew]))
    res = false;
  else
    iE = iEnew;
  histo_count[iE]++;
  ln_density[iE] += f;
  return res;
}

void Wang_Landau_histograms::update_iE(int E) { iE = (E - Emin) / dE; }

int Wang_Landau_histograms::find_iE(int E) { return (E - Emin) / dE; }

const bool Wang_Landau_histograms::E_not_in_range(int const E) {
  if (E < Emin or E >= Emax)
    return true;
  return false;
}

const bool Wang_Landau_histograms::is_better(int const E, int const deltaE) {
  if ((E - Emin) * deltaE <= 0)
    return true;
  return false;
}

#endif
