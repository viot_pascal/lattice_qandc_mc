# -*- coding: utf-8 -*-
"""
Created on Thu Feb 12 12:04:20 2015

@author: messio
"""

#from the name of a file with two columns: 
#L and max_Cv
#plot max_Cv as a function of L

import sys
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

def puiss(x,a,b):
    return a*x**b

L,Cvmax,T = np.loadtxt(sys.argv[1],unpack=True)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('L')
plt.ylabel('Max_Cv')
plt.plot(L,Cvmax,'o')
res1,res2=curve_fit(puiss,L,Cvmax)
print 'puissance=',res1[1],' pm',res2[1,1]
res1,res2=curve_fit(puiss,L[2:],Cvmax[2:])
print 'avec deux points en moins'
print 'puissance=',res1[1],' pm',res2[1,1]
plt.plot(L,puiss(L,res1[0],res1[1]),'-g')
plt.title('fit by a power law')
plt.xlim([min(L),max(L)])
plt.ylim([min(Cvmax),max(Cvmax)])
plt.show()



