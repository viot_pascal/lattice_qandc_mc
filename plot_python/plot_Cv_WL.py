"""
first argument: lng_E file name
second argument: Tmin
third argument: Tmax
last argument: nT
optional : 'Cv', 'lngE', 'pE' or 'E' : 
   to have only one of the four possible graphs
"""

import sys
import matplotlib.pyplot as plt
import numpy as np

E,ln,hist=np.loadtxt(sys.argv[1],unpack=True)
Tmin = float(sys.argv[2])
Tmax = float(sys.argv[3])
nT   = int(  sys.argv[4])
Ns   = int(  sys.argv[1].split('_Ns_')[1].split('_')[0] )
nE  = len(E)
Em  = np.zeros(nT) #average E as function of T
E2m = np.zeros(nT) #average E^2 as function of T
Cvm = np.zeros(nT) #specific heat
pE  = np.empty((nT,nE)) #probability of having E
T   = np.linspace(Tmin,Tmax,nT) #temperature

for it in range(nT):
  t=T[it]
  Zt   = 0
  Emt  = 0
  E2mt = 0
  beta = 1/t
  for i in range(nE):
      fact=np.exp(-beta*(E[i]-E[0])+ln[i])
      Zt+=fact
      Emt+=E[i]*fact
      E2mt+=E[i]**2*fact
      pE[it][i]=fact
  Emt  /=Zt
  E2mt /=Zt
  Em[it] = Emt/Ns
  E2m[it]= E2mt/(Ns**2)
  Cvm[it]= (E2mt-Emt**2)*beta**2/Ns
  for i in range(nE):
    pE[it][i] /= Zt
E/=Ns

if len(sys.argv)==5:
  fig, axes = plt.subplots(1, 4, figsize=(20, 4))
  plt.subplot(144)
  for i in range(nT):
    plt.plot(E,pE[i],'r-')
  plt.title('p(E)')
  plt.subplot(143)
  plt.plot(E,ln,'r-')
  plt.title('ln(g(E))')
  ax =plt.subplot(141)
  plt.plot(T,Em,'bo')
  plt.title('E(T)')
  ax =plt.subplot(142)
  plt.plot(T,Cvm,'bo')
  plt.title('Cv(T)')
  print 'Ns=',Ns, 'L=',Ns**(1/3.),', max(Cv)=',max(Cvm), ', T(max)=',T[Cvm.argmax()]
  print Ns**(1/3.),max(Cvm),T[Cvm.argmax()]
elif sys.argv[5]=='Cv':
  plt.plot(T,Cvm,'bo')
  plt.xlabel('T')
  plt.ylabel('Cv')
  print 'Ns=',Ns, 'L=',Ns**(1/3.),', max(Cv)=',max(Cvm), ', T(max)=',T[Cvm.argmax()]
  print Ns**(1/3.),max(Cvm),T[Cvm.argmax()]
elif sys.argv[5]=='E':
  plt.plot(T,Em,'bo')
  plt.xlabel('T')
  plt.ylabel('E')
elif sys.argv[5]=='lngE':
  plt.plot(T,ln,'b-')
  plt.xlabel('T')
  plt.ylabel('ln(g(E))')
elif sys.argv[5]=='pE':
  for i in range(nT):
    plt.plot(E,pE[i],'r-')
  plt.xlabel('T')
  plt.ylabel('p(E)')
else:
  print 'unknown option'
print 'Ns=',Ns
    
plt.title(sys.argv[1])
plt.show()