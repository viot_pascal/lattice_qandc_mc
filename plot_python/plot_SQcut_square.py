from numpy.random import uniform, seed
from matplotlib.mlab import griddata
import matplotlib.pyplot as plt
from pylab import *
f= open(sys.argv[1], 'r')
f.readline()
x=[]
y=[]
z=[]
zmax=0.1
fact=0
a=(f.readline()).split()
#print a
while len(a)>=2:
  x.append(float(a[0]))
  y.append(float(a[1]))
  if(float(a[1])>fact):
    fact=float(a[1])
  if(float(a[2])<zmax):
    z.append(float(a[2]))
  else:
    z.append(zmax)
  a=(f.readline()).split()
f.close()
n=len(x)
fact/=pi*(sqrt(2)+2)
for i in range(n):
  x[i]*=fact
  
# define grid triangle.
ly=y[n-1]
lx=fact*pi*(sqrt(2)+2)
xi = linspace(0,lx,200)
yi = linspace(0,y[n-1],200)

# grid the data.
zi = griddata(x,y,z,xi,yi)

#peculiar points
G1=(0,y[0],0)
G2=(0,y[n-1],0)
K1=(fact*pi*sqrt(2),y[0],0)
K2=(fact*pi*sqrt(2),y[n-1],0)
M1=(fact*pi*(sqrt(2)+1),y[0],0)
M2=(fact*pi*(sqrt(2)+1),y[n-1],0)
G3=(fact*pi*(sqrt(2)+2),y[0],0)
G4=(fact*pi*(sqrt(2)+2),y[n-1],0)

##point of the BZ
#A=(2*pi/3,2*pi/sqrt(3),0)
#B=(4*pi/3,0,0)
#C=(2*pi/3,-2*pi/sqrt(3),0)
#D=(-2*pi/3,-2*pi/sqrt(3),0)
#E=(-4*pi/3,0,0)
#F=(-2*pi/3,2*pi/sqrt(3),0)
plot([K1[0],K2[0]],[K1[1],K2[1]],color='w',linewidth=1)
plot([M1[0],M2[0]],[M1[1],M2[1]],color='w',linewidth=1)
plot([G1[0],G2[0]],[G1[1],G2[1]],color='w',linewidth=1)
plot([G3[0],G4[0]],[G3[1],G4[1]],color='w',linewidth=1)
#plot([2*A[0],2*B[0],2*C[0],2*D[0],2*E[0],2*F[0],2*A[0]],[2*A[1],2*B[1],2*C[1],2*D[1],2*E[1],2*F[1],2*A[1]],color='k',linewidth=2)

#CS = plt.contourf(xi,yi,zi,36,cmap=plt.cm.jet)

imgplot =plt.imshow(zi, interpolation='bilinear', origin='lower',
                cmap=cm.gray, extent=(0,lx,0,y[n-1]))
imgplot.set_cmap('gnuplot')
xticks([G1[0],K1[0],M1[0],G3[0]],[r'$\Gamma$', r'$K_e$', r'$M_e$', r'$\Gamma$'],fontsize=20)
yticks(fontsize=20)
ylabel(r'$\omega$',fontsize=20)

# draw colorbar
cb = plt.colorbar() # grab the Colorbar instance
for t in cb.ax.get_yticklabels():
     t.set_fontsize(20)


## save figure
#plt.savefig(sys.argv[1].replace('.dat','.png'))

plt.show()

