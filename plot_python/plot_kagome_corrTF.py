from numpy.random import uniform, seed
from matplotlib.mlab import griddata
import matplotlib.pyplot as plt
from pylab import *
import sys

f= open(sys.argv[1], 'r')
f.readline()
x=[]
y=[]
z=[]
sum=0
a=(f.readline()).split()
n=len(a)
#print a
while len(a)==n:
  x.append(float(a[0]))
  y.append(float(a[1]))
  z.append(float(a[2]))
  sum+=float(a[2])
  a=(f.readline()).split()
f.close()

n=len(x)
print 'sum ok SQ=NQ*',sum/n

for i in range(n):
  z.append(z[i])
  x.append(x[i])
  y.append(y[i]-8*pi/sqrt(3))

for i in range(2*n):
  z.append(z[i])
  x.append(x[i]-4*pi)
  y.append(y[i]+4*pi/sqrt(3))

# define grid triangle.
l=2*pi/sqrt(3)*2.4
xi = linspace(-l,l,100)
yi = linspace(-l,l,100)

# grid the data.
zi = griddata(x,y,z,xi,yi)

#point of the BZ
A=(2*pi/3,2*pi/sqrt(3),0)
B=(4*pi/3,0,0)
C=(2*pi/3,-2*pi/sqrt(3),0)
D=(-2*pi/3,-2*pi/sqrt(3),0)
E=(-4*pi/3,0,0)
F=(-2*pi/3,2*pi/sqrt(3),0)
plot([A[0],B[0],C[0],D[0],E[0],F[0],A[0]],[A[1],B[1],C[1],D[1],E[1],F[1],A[1]],color='w',linewidth=2)
plot([2*A[0],2*B[0],2*C[0],2*D[0],2*E[0],2*F[0],2*A[0]],[2*A[1],2*B[1],2*C[1],2*D[1],2*E[1],2*F[1],2*A[1]],color='w',linewidth=1)

#CS = plt.contourf(xi,yi,zi,36,cmap=plt.cm.jet)

imgplot =plt.imshow(zi, interpolation='bilinear', origin='lower',
                cmap=cm.gray, extent=(-l,l,-l,l))
imgplot.set_cmap('gnuplot')
#xticks([-2*pi,-pi,0,pi,2*pi],[r'$-2\pi$', r'$-\pi$', r'$0$', r'$\pi$', r'$2\pi$'],fontsize=20)
#yticks([-2*pi,-pi,0,pi,2*pi],[r'$-2\pi$', r'$-\pi$', r'$0$', r'$\pi$', r'$2\pi$'],fontsize=20)
xticks([])
yticks([])

# draw colorbar
cb = plt.colorbar() # grab the Colorbar instance
for t in cb.ax.get_yticklabels():
     t.set_fontsize(20)

##save figure
#plt.savefig(sys.argv[1].replace('.dat','.png'))

plt.show()

