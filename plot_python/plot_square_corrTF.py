from numpy.random import uniform, seed
from matplotlib.mlab import griddata
import matplotlib.pyplot as plt
from pylab import *
import sys

f= open(sys.argv[1], 'r')
f.readline()
x=[]
y=[]
z=[]
sum=0
a=(f.readline()).split()
#print a
n=len(a)
if len(a)>0:
  while len(a)==n:
    x.append(float(a[0]))
    y.append(float(a[1]))
    z.append(float(a[2]))
    sum+=float(a[2])
    #z.append(1.)
    #sum+=1.0
    a=(f.readline()).split()
f.close()

n=len(x)
print 'sum of SQ=',sum
for i in range(n):
  z[i]*=n/(4.*pi*pi*sum)
  #print n/(4.*pi*pi*sum)
  #print z[i]

z.extend(z)
for i in range(n):
  x.append(x[i]-2*pi)
  y.append(y[i])

z.extend(z)
for i in range(2*n):
  x.append(x[i])
  y.append(y[i]-2*pi)

#point of the BZ
A=(pi,pi,0)
B=(pi,-pi)
C=(-pi,-pi,0)
D=(-pi,pi,0)
plot([A[0],B[0],C[0],D[0],A[0]],[A[1],B[1],C[1],D[1],A[1]],color='w',linewidth=1)

  
# define grid square.
#xi = linspace(0,6.3,100)
#yi = linspace(0,6.3,100)
# define grid triangle.
l=pi*1.15
#print l
xi = linspace(-l,l,100)
yi = linspace(-l,l,100)

# grid the data.
zi = griddata(x,y,z,xi,yi)
yticks([])
xticks([])
#CS = plt.contour(xi,yi,zi,15,linewidths=0.5,colors='k')

imgplot =plt.imshow(zi, interpolation='bilinear', origin='lower',
                cmap=cm.gray, extent=(-l,l,-l,l))
imgplot.set_cmap('gnuplot')
xticks([])
yticks([])

# draw colorbar
cb = plt.colorbar() # grab the Colorbar instance
for t in cb.ax.get_yticklabels():
     t.set_fontsize(20)

## save figure
#plt.savefig(sys.argv[1].replace('.dat','.png'))
plt.show()

