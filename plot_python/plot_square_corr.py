from numpy.random import uniform, seed
from matplotlib.mlab import griddata
import matplotlib.pyplot as plt
from pylab import *
import sys

f= open(sys.argv[1], 'r')
f.readline()
x=[]
y=[]
z=[]
a=(f.readline()).split()
maxx=0
minx=100
maxy=0
miny=100
#print a
n=len(a)
if len(a)>0:
  while len(a)==n:
    x.append(float(a[0]))
    y.append(float(a[1]))
    z.append(float(a[2]))
    if float(a[0])<minx:
      minx=float(a[0])
    if float(a[0])>maxx:
      maxx=float(a[0])
    if float(a[1])<miny:
      miny=float(a[0])
    if float(a[1])>maxy:
      maxy=float(a[0])
    a=(f.readline()).split()
f.close()

n=len(x)

#z.extend(z)
#for i in range(n):
  #x.append(x[i]-2*pi)
  #y.append(y[i])

#z.extend(z)
#for i in range(2*n):
  #x.append(x[i])
  #y.append(y[i]-2*pi)

##point of the BZ
#A=(pi,pi,0)
#B=(pi,-pi)
#C=(-pi,-pi,0)
#D=(-pi,pi,0)
#plot([A[0],B[0],C[0],D[0],A[0]],[A[1],B[1],C[1],D[1],A[1]],color='w',linewidth=1)

# define grid triangle.
xi = linspace(minx,maxx,100)
yi = linspace(miny,maxy,100)

# grid the data.
zi = griddata(x,y,z,xi,yi)
#CS = plt.contour(xi,yi,zi,15,linewidths=0.5,colors='k')

imgplot =plt.imshow(zi, interpolation='bilinear', origin='lower',
                cmap=cm.gray, extent=(minx,maxx,miny,maxy))
imgplot.set_cmap('hot')

#CS = plt.contourf(xi,yi,zi,36,cmap=plt.cm.jet)
plt.colorbar() # draw colorbar
plt.savefig(sys.argv[1].replace('.dat','.png'))
#plt.colorbar.ColorbarBase(x,)
plt.show()

