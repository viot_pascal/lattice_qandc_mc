"""
first argument: observable file name
second argument: Tmin
third argument: Tmax
last argument: nT
optional : 'obs', 'obsE', 'chi', 'pE : 
   to have only one of the two possible graphs
"""

import sys
import re
import matplotlib.pyplot as plt
import numpy as np

temp=sys.argv[1].split('/')
ntemp=len(temp)-1
obs_name = temp[ntemp].split('_')[1]       #name of the observable 
E,ln,hist=np.loadtxt(re.sub(obs_name, 'lngE', sys.argv[1]),unpack=True)
Qa,Q,Q2,histo = np.loadtxt(sys.argv[1],unpack=True)
Tmin = float(sys.argv[2])
Tmax = float(sys.argv[3])
nT   = int(  sys.argv[4])
Ns   = int(  sys.argv[1].split('_Ns_')[1].split('_')[0] )
nE   = len(E)
Qam  = np.zeros(nT) # average of the abs value of Q as a function of T
Qm   = np.zeros(nT) # average Q as function of T
Q2m  = np.zeros(nT) # average Q^2 as function of T
Chia = np.zeros(nT) # susceptibility of Q with || as function of T
Chi  = np.zeros(nT) # susceptibility of Q without || as function of T
pE   = np.empty((nT,nE)) #probability of having E
T    = np.linspace(Tmin,Tmax,nT) #temperature

for it in range(nT):
  t=T[it]
  Zt   = 0
  Qamt = 0
  Qmt  = 0 #observable at temperature T
  Q2mt = 0 #observable squared at temperature T
  beta = 1/t
  for i in range(nE):
      fact=np.exp(-beta*(E[i]-E[0])+ln[i])
      Zt+=fact
      Qamt+=Qa[i]*fact
      Qmt+=Q[i]*fact
      Q2mt+=Q2[i]*fact
      pE[it][i]=fact
  Qamt /=Zt
  Qmt  /=Zt
  Q2mt /=Zt
  Qam[it]= Qamt/Ns     # Q by site
  Qm[it] = Qmt/Ns      # Q by site
  Q2m[it]= Q2mt/(Ns**2)# Q by site
  Chia[it]= (Q2mt-Qamt**2)*beta/Ns
  Chi[it] = (Q2mt-Qmt**2 )*beta/Ns
  for i in range(nE):
    pE[it][i] /= Zt
E/=Ns

if len(sys.argv)==5:
  fig, axes = plt.subplots(1, 4, figsize=(20, 4))
  plt.subplot(144)
  for i in range(nT):
    plt.plot(E,pE[i],'r-')
  plt.title('p(E)')
  plt.xlabel('E')
  ax =plt.subplot(143)
  plt.plot(E,Q,'b-')
  plt.plot(E,Qa,'r-')
  plt.xlabel('E')
  plt.title(obs_name+'(E)')
  ax =plt.subplot(141)
  plt.plot(T,Qm,'bo')
  plt.plot(T,Qam,'ro')
  plt.xlabel('T')
  plt.title(obs_name+'(T)')
  ax =plt.subplot(142)
  plt.plot(T,Chi,'bo')
  plt.plot(T,Chia,'ro')
  #plt.plot(T,6/T,'-g')
  plt.xlabel('T')
  plt.title('Chi(T)')
  print 'Ns=',Ns, 'L=',Ns**(1/3.),' max(Chi)=',max(Chi), 'T(max)=',T[Chi.argmax()]
#  print 'Ns=',Ns, 'L=',Ns**(1/3.),' max(Chip)=',max(Chip), 'T(max)=',T[Chip.argmax()]
  print Ns**(1/3.),max(Chi),T[Chi.argmax()]
#  print Ns**(1/3.),max(Chip),T[Chip.argmax()]
elif sys.argv[5]=='chi':
  plt.plot(T,Chi,'bo')
  plt.plot(T,Chia,'ro')
  plt.xlabel('T')
  plt.ylabel('Chi(T)')
  print 'Ns=',Ns, 'L=',Ns**(1/3.),' max(Chi)=',max(Chi), 'T(max)=',T[Chi.argmax()]
#  print 'Ns=',Ns, 'L=',Ns**(1/3.),' max(Chip)=',max(Chip), 'T(max)=',T[Chip.argmax()]
  print Ns**(1/3.),max(Chi),T[Chi.argmax()]
#  print Ns**(1/3.),max(Chip),T[Chip.argmax()]
elif sys.argv[5]=='obs':
  plt.plot(T,Qm,'bo')
  plt.plot(T,Qam,'ro')
  plt.xlabel('Q(T)')
  plt.ylabel(obs_name)
elif sys.argv[5]=='obsE':
  plt.plot(E,Q,'b-')
  plt.plot(E,Qa,'r-')
  plt.xlabel(obs_name+'(E)')
  plt.ylabel(obs_name)
elif sys.argv[5]=='pE':
  for i in range(nT):
    plt.plot(E,pE[i],'r-')
  plt.xlabel('T')
  plt.ylabel('p(E)')
else:
  print 'unknown option'

print 'Ns=',Ns
plt.title(sys.argv[1])
plt.show()
