from numpy.random import uniform, seed
from matplotlib.mlab import griddata
import matplotlib.pyplot as plt
from pylab import *
f= open(sys.argv[1], 'r')
f.readline()
q=[]
w=[]
Sq=[]
S=.5
temp=sys.argv[1].split('_')
for i in range(len(temp)):
  if temp[i]=="S":
    S=float(temp[i+1])
print 'S=',S
Sqmax=S
fact=0
a=(f.readline()).split()
#print a
while len(a)>=3:
  q.append(float(a[0]))
  w.append(float(a[1]))
  if(float(a[1])>fact):
    fact=float(a[1])
  if(float(a[2])<Sqmax):
    Sq.append(float(a[2]))
  else:
    Sq.append(Sqmax)
  a=(f.readline()).split()
f.close()
n=len(q)
fact/=pi*(sqrt(3)+1)
for i in range(n):
  q[i]*=fact
  
# define grid triangle.
ly=w[n-1]
lx=fact*pi*(sqrt(3)+1)
xi = linspace(0,lx,200)
yi = linspace(0,w[n-1],200)

# grid the data.
zi = griddata(q,w,Sq,xi,yi)

#peculiar points
G1=(0,w[0],0)
G2=(0,w[n-1],0)
K1=(fact*2*pi/sqrt(3),w[0],0)
K2=(fact*2*pi/sqrt(3),w[n-1],0)
M1=(fact*3*pi/sqrt(3),w[0],0)
M2=(fact*3*pi/sqrt(3),w[n-1],0)
G3=(fact*(3*pi/sqrt(3)+pi),w[0],0)
G4=(fact*pi*(sqrt(3)+1),w[n-1],0)

plot([K1[0],K2[0]],[K1[1],K2[1]],color='w',linewidth=1)
plot([M1[0],M2[0]],[M1[1],M2[1]],color='w',linewidth=1)
plot([G1[0],G2[0]],[G1[1],G2[1]],color='w',linewidth=1)
plot([G3[0],G4[0]],[G3[1],G4[1]],color='w',linewidth=1)

imgplot =plt.imshow(zi, interpolation='bilinear', origin='lower',cmap=cm.gray, extent=(0,lx,0,w[n-1]))
imgplot.set_cmap('gnuplot')

# draw colorbar
cb = plt.colorbar() # grab the Colorbar instance
for t in cb.ax.get_yticklabels():
     t.set_fontsize(20)
xticks([G1[0],K1[0],M1[0],G3[0]],[r'$\Gamma$', r'$K_e$', r'$M_e$', r'$\Gamma$'],fontsize=20)
yticks(fontsize=20)
ylabel(r'$\omega$',fontsize=20)

## save figure
#plt.savefig(sys.argv[1].replace('.dat','.png'))

plt.show()

