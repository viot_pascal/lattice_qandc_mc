from numpy.random import uniform, seed
from matplotlib.mlab import griddata
import matplotlib.pyplot as plt
from pylab import *
import sys

f= open(sys.argv[1], 'r')
f.readline()
x0=[]
y0=[]
z0=[]
sum=0
a=(f.readline()).split()
n=len(a)
#print a
while len(a)==n:
  x0.append(float(a[0]))
  y0.append(float(a[1]))
  z0.append(float(a[2]))
  sum+=float(a[2])
  a=(f.readline()).split()
f.close()

n=len(x0)
print 'sum ok SQ=NQ*',sum/n
x=[]
y=[]
z=[]

for i in range(n):
  z.append(z0[i])
  x.append(x0[i])
  y.append(y0[i])

for i in range(n):
  z.append(z0[i])
  x.append(x0[i]-2*pi)
  y.append(y0[i]+2*pi/sqrt(3))

for i in range(n):
  z.append(z0[i])
  x.append(x0[i]-2*pi)
  y.append(y0[i]-2*pi/sqrt(3))

for i in range(n):
  z.append(z0[i])
  x.append(x0[i])
  y.append(y0[i]-4*pi/sqrt(3))

# define grid triangle.
l=2*pi/sqrt(3)*1.2
xi = linspace(-l,l,100)
yi = linspace(-l,l,100)

# grid the data.
zi = griddata(x,y,z,xi,yi)

#point of the BZ
A=(2*pi/3,2*pi/sqrt(3),0)
B=(4*pi/3,0,0)
C=(2*pi/3,-2*pi/sqrt(3),0)
D=(-2*pi/3,-2*pi/sqrt(3),0)
E=(-4*pi/3,0,0)
F=(-2*pi/3,2*pi/sqrt(3),0)
plot([A[0],B[0],C[0],D[0],E[0],F[0],A[0]],[A[1],B[1],C[1],D[1],E[1],F[1],A[1]],color='w',linewidth=1)

#CS = plt.contourf(xi,yi,zi,36,cmap=plt.cm.jet)

imgplot =plt.imshow(zi, interpolation='bilinear', origin='lower',
                cmap=cm.gray, extent=(-l,l,-l,l))
imgplot.set_cmap('gnuplot')

# draw colorbar
#xticks([-2*pi,-pi,0,pi,2*pi],[r'$-2\pi$', r'$-\pi$', r'$0$', r'$\pi$', r'$2\pi$'],fontsize=20)
#yticks([-2*pi,-pi,0,pi,2*pi],[r'$-2\pi$', r'$-\pi$', r'$0$', r'$\pi$', r'$2\pi$'],fontsize=20)
xticks([])
yticks([])

# draw colorbar
cb = plt.colorbar() # grab the Colorbar instance
for t in cb.ax.get_yticklabels():
     t.set_fontsize(20)

## save figure
#plt.savefig(sys.argv[1].replace('.dat','.png'))

plt.show()

